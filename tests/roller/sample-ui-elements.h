/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2011 Robert Bosch Car Multimedia GmbH
 * Author: Tomeu Vizoso <tomeu.vizoso@collabora.co.uk>
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <clutter/clutter.h>

#define STATUS_BAR_FILE_NAME    "StatusBar_DRAGON_plain.png"
#define DEFAULT_ICON_FILE_NAME  "icon_music.png"
#define ICON1_FILE_NAME         "icon_email.png"
#define ICON2_FILE_NAME         "icon_music.png"
#define VIDEO1_FILE_NAME        "01_appl_launcher_2009.03.09.avi"
#define VIDEO2_FILE_NAME        "03_content_roll_2009.03.09.avi"
#define COVER1_FILE_NAME        "cover1.jpg"
#define COVER2_FILE_NAME        "cover2.jpg"
#define COVER3_FILE_NAME        "cover3.jpg"
#define COVER4_FILE_NAME        "cover4.jpg"
#define THUMB1_FILE_NAME        "01_appl_launcher_2009.03.09.png"
#define THUMB2_FILE_NAME        "03_content_roll_2009.03.09.png"
#define LONG_TEXT               "long text for item %i\nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \n"

enum
{
  COLUMN_NAME,
  COLUMN_ICON,
  COLUMN_LABEL,
  COLUMN_TOGGLE,
  COLUMN_VIDEO,
  COLUMN_EXTRA_HEIGHT,
  COLUMN_COVER,
  COLUMN_THUMB,
  COLUMN_LONG_TEXT,

  COLUMN_LAST
};

ClutterActor *create_status_bar (void);
ClutterActor *create_separator (void);
ClutterActor *create_separator_group (void);
ClutterModel *create_model (guint num_of_items);
ClutterActor *create_scroll_view (void);
gulong get_elastic_mode (void);
