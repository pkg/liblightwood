/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2011 Robert Bosch Car Multimedia GmbH
 * Author: Tomeu Vizoso <tomeu.vizoso@collabora.co.uk>
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sample-glow-shader.h"

G_DEFINE_TYPE (SampleGlowShader, sample_glow_shader, CLUTTER_TYPE_SHADER_EFFECT)

static gchar *shader_source =
"uniform sampler2D tex;\n"
"uniform float texstep;\n"
"\n"
"void main () {\n"
"    float offset[2];\n"
"    float weight[2];\n"
"    vec4 color = texture2D(tex, cogl_tex_coord_in[0].st);\n"
"    float local_offset;\n"
"\n"
/* Cannot use array constructors in GLSL < 1.20 and Cogl doesn't allow us to set
   the version */
"    offset[0] = 40.0;\n"
"    offset[1] = 60.0;\n"
"    weight[0] = 0.1;\n"
"    weight[1] = 0.05;\n"
"\n"
"    for (int i = 0; i < 2; i++) {\n"
"        local_offset = texstep * offset[i];\n"
"        // bottom right\n"
"        color += texture2D(tex, cogl_tex_coord_in[0].st + vec2(local_offset, local_offset)) * weight[i];\n"
"        // top left\n"
"        color += texture2D(tex, cogl_tex_coord_in[0].st - vec2(local_offset, local_offset)) * weight[i];\n"
"        // top right\n"
"        color += texture2D(tex, cogl_tex_coord_in[0].st + vec2(local_offset, -local_offset)) * weight[i];\n"
"        // bottom left\n"
"        color += texture2D(tex, cogl_tex_coord_in[0].st - vec2(local_offset, -local_offset)) * weight[i];\n"
"        // left\n"
"        color += texture2D(tex, cogl_tex_coord_in[0].st - vec2(local_offset, 0)) * weight[i];\n"
"        // right\n"
"        color += texture2D(tex, cogl_tex_coord_in[0].st + vec2(local_offset, 0)) * weight[i];\n"
"        // top\n"
"        color += texture2D(tex, cogl_tex_coord_in[0].st - vec2(0, local_offset)) * weight[i];\n"
"        // bottom\n"
"        color += texture2D(tex, cogl_tex_coord_in[0].st + vec2(0, local_offset)) * weight[i];\n"
"    }\n"
"\n"
"    gl_FragColor = color;\n"
"}";

static gboolean
sample_glow_shader_pre_paint (ClutterEffect *effect)
{
  ClutterShaderEffect *shader = CLUTTER_SHADER_EFFECT (effect);
  ClutterActor *actor = clutter_actor_meta_get_actor (CLUTTER_ACTOR_META (effect));
  float tex_width, tex_height, tex_step;

  if (!clutter_actor_meta_get_enabled (CLUTTER_ACTOR_META (effect)))
    return FALSE;

  clutter_shader_effect_set_shader_source (shader, shader_source);

  tex_width = clutter_actor_get_width (actor);
  tex_height = clutter_actor_get_height (actor);
  tex_step = 1 / (tex_width * tex_height);

  clutter_shader_effect_set_uniform (shader, "tex", G_TYPE_INT, 1, 0);
  clutter_shader_effect_set_uniform (shader, "texstep", G_TYPE_FLOAT, 1, tex_step);

  return CLUTTER_EFFECT_CLASS (sample_glow_shader_parent_class)->pre_paint (effect);
}

static void
sample_glow_shader_class_init (SampleGlowShaderClass *klass)
{
  ClutterEffectClass *shader_class = CLUTTER_EFFECT_CLASS (klass);

  shader_class->pre_paint = sample_glow_shader_pre_paint;
}

static void
sample_glow_shader_init (SampleGlowShader *self)
{
}

ClutterEffect *
sample_glow_shader_new (void)
{
  return g_object_new (TYPE_SAMPLE_GLOW_SHADER, NULL);
}
