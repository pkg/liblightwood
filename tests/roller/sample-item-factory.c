/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2011 Robert Bosch Car Multimedia GmbH
 * Author: Tomeu Vizoso <tomeu.vizoso@collabora.co.uk>
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sample-item-factory.h"
#include "sample-item.h"

static void mx_item_factory_iface_init (MxItemFactoryIface *iface);

G_DEFINE_TYPE_WITH_CODE (SampleItemFactory, sample_item_factory, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (MX_TYPE_ITEM_FACTORY, mx_item_factory_iface_init))

#define SAMPLE_ITEM_FACTORY_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), TYPE_SAMPLE_ITEM_FACTORY, SampleItemFactoryPrivate))

struct _SampleItemFactoryPrivate
{
  GType item_type;
  ClutterModel *model;
};

enum {
  PROP_0,

  PROP_ITEM_TYPE,
  PROP_MODEL,
};

static ClutterActor*
item_factory_create (MxItemFactory *factory)
{
  SampleItemFactoryPrivate *priv = SAMPLE_ITEM_FACTORY (factory)->priv;
  ClutterActor *item;

  item = g_object_new (priv->item_type, NULL);

  if (g_object_class_find_property (G_OBJECT_GET_CLASS (item),
                                    "model") != NULL)
    g_object_set (item, "model", priv->model, NULL);

  return item;
}

static void
mx_item_factory_iface_init (MxItemFactoryIface *iface)
{
  iface->create = item_factory_create;
}


static void
sample_item_factory_get_property (GObject    *object,
                                  guint       property_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  SampleItemFactoryPrivate *priv = SAMPLE_ITEM_FACTORY (object)->priv;

  switch (property_id)
    {
    case PROP_ITEM_TYPE:
      g_value_set_gtype (value, priv->item_type);
      break;
    case PROP_MODEL:
      g_value_set_object (value, priv->model);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
sample_item_factory_set_property (GObject      *object,
                                  guint         property_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  SampleItemFactoryPrivate *priv = SAMPLE_ITEM_FACTORY (object)->priv;

  switch (property_id)
    {
    case PROP_ITEM_TYPE:
      if (priv->item_type != g_value_get_gtype (value))
        {
          priv->item_type = g_value_get_gtype (value);
          g_object_notify (object, "item-type");
        }
      break;
    case PROP_MODEL:
      if (priv->model != g_value_get_object (value))
        {
          priv->model = g_value_get_object (value);
          g_object_notify (object, "model");
        }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
sample_item_factory_dispose (GObject *object)
{
  G_OBJECT_CLASS (sample_item_factory_parent_class)->dispose (object);
}

static void
sample_item_factory_finalize (GObject *object)
{
  G_OBJECT_CLASS (sample_item_factory_parent_class)->finalize (object);
}

static void
sample_item_factory_class_init (SampleItemFactoryClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GParamSpec *pspec;

  g_type_class_add_private (klass, sizeof (SampleItemFactoryPrivate));

  object_class->get_property = sample_item_factory_get_property;
  object_class->set_property = sample_item_factory_set_property;
  object_class->dispose = sample_item_factory_dispose;
  object_class->finalize = sample_item_factory_finalize;

  pspec = g_param_spec_gtype ("item-type",
                              "Item GType",
                              "Item GType to instantiate",
                              CLUTTER_TYPE_ACTOR,
                              G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_ITEM_TYPE, pspec);

  pspec = g_param_spec_object ("model",
                               "ClutterModel",
                               "ClutterModel",
                               CLUTTER_TYPE_MODEL,
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_MODEL, pspec);
}

static void
sample_item_factory_init (SampleItemFactory *self)
{
  self->priv = SAMPLE_ITEM_FACTORY_PRIVATE (self);
}

SampleItemFactory *
sample_item_factory_new (void)
{
  return g_object_new (TYPE_SAMPLE_ITEM_FACTORY, NULL);
}
