/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __SAMPLE_ITEM_H__
#define __SAMPLE_ITEM_H__

#include <clutter/clutter.h>
#include <cogl/cogl.h>
#include <mx/mx.h>

#define ARROW_UP_FILE_NAME        "arrow-up.png"
#define ARROW_DOWN_FILE_NAME      "arrow-down.png"

G_BEGIN_DECLS

#define TYPE_SAMPLE_ITEM             (sample_item_get_type ())
#define SAMPLE_ITEM(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SAMPLE_ITEM, SampleItem))
#define SAMPLE_ITEM_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_SAMPLE_ITEM, SampleItemClass))
#define IS_SAMPLE_ITEM(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SAMPLE_ITEM))
#define IS_SAMPLE_ITEM_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_SAMPLE_ITEM))
#define SAMPLE_ITEM_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SAMPLE_ITEM, SampleItemClass))

typedef struct _SampleItem        SampleItem;
typedef struct _SampleItemClass   SampleItemClass;

enum
{
  ARROW_WHITE,
  ARROW_GREY,
  ARROW_HIDDEN,

  ARROW_LAST
};

struct _SampleItem
{
  ClutterGroup group;

  ClutterActor *icon;
  ClutterActor *label;
  ClutterActor *arrow_up;
  ClutterActor *arrow_down;
  ClutterActor *extra_separator;

  ClutterModel *model;

  gboolean expanded;

  ClutterEffect *glow_effect_1;
  ClutterEffect *glow_effect_2;
};

struct _SampleItemClass
{
  ClutterGroupClass        parent_class;
};


GType          sample_item_get_type         (void) G_GNUC_CONST;
ClutterActor*  sample_item_new              (void);

G_END_DECLS

#endif /* __SAMPLE_ITEM_H__ */

