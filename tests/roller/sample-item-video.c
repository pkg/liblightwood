/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2011 Robert Bosch Car Multimedia GmbH
 * Author: Tomeu Vizoso <tomeu.vizoso@collabora.co.uk>
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sample-item-video.h"

#include <string.h>
#include <clutter-gst/clutter-gst.h>

G_DEFINE_TYPE_WITH_CODE (SampleItemVideo, sample_item_video, CLUTTER_TYPE_GROUP, );

enum
{
  PROP_0,

  PROP_LABEL,
  PROP_VIDEO,
};

static void
sample_item_video_get_property (GObject    *object,
                          guint       property_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  SampleItemVideo *item = SAMPLE_ITEM_VIDEO (object);

  switch (property_id)
    {
    case PROP_LABEL:
      g_value_set_string (value,
          g_strdup (clutter_text_get_text (CLUTTER_TEXT (item->label))));
      break;
    case PROP_VIDEO:
      g_value_take_string (value,
          clutter_gst_playback_get_uri (item->player));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
sample_item_video_set_property (GObject      *object,
                                guint         property_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  SampleItemVideo *item = SAMPLE_ITEM_VIDEO (object);

  switch (property_id)
    {
    case PROP_LABEL:
      clutter_text_set_text (CLUTTER_TEXT (item->label),
                             g_value_get_string (value));
      break;
    case PROP_VIDEO:
      {
        gchar *uri = clutter_gst_playback_get_uri (item->player);
        if (uri == NULL || strcmp (g_value_get_string (value), uri) != 0)
          {
            gchar *scheme = NULL;
            if (g_value_get_string (value) != NULL)
              scheme = g_uri_parse_scheme (g_value_get_string(value));

            /* if it's not really a uri assume it's a filename */
            if (scheme == NULL)
              clutter_gst_playback_set_filename (item->player,
                                          g_value_get_string (value));
            else
              clutter_gst_playback_set_uri (item->player,
                                          g_value_get_string (value));
            g_free (scheme);
          }
          g_free (uri);
        }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
eos_cb (ClutterGstPlayer *player, gpointer user_data)
{
  clutter_gst_player_set_playing (player, TRUE);
}

static void
sample_item_video_class_init (SampleItemVideoClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GParamSpec *pspec;

  object_class->get_property = sample_item_video_get_property;
  object_class->set_property = sample_item_video_set_property;

  pspec = g_param_spec_string ("label",
                               "label",
                               "Text for the label",
                               NULL,
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_LABEL, pspec);

  pspec = g_param_spec_string ("video",
                               "video",
                               "Video URI",
                               NULL,
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_VIDEO, pspec);
}

static void
sample_item_video_init (SampleItemVideo *sample_item_video)
{
  ClutterActor *line;
  ClutterColor text_color = { 255, 255, 255, 255 };
  ClutterColor line_color = {0xFF, 0xFF, 0xFF, 0x33};

  sample_item_video->player = clutter_gst_playback_new ();
  g_signal_connect (sample_item_video->player, "eos", G_CALLBACK (eos_cb), NULL);

  sample_item_video->video = g_object_new (CLUTTER_TYPE_ACTOR,
      "width", 120,
      "height", 120,
      "x", 18,
      "y", 0,
      "content", g_object_new (CLUTTER_GST_TYPE_ASPECTRATIO,
                     "player", sample_item_video->player,
                     NULL),
      NULL);
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item_video), sample_item_video->video);
  clutter_actor_show (sample_item_video->video);

  sample_item_video->label = clutter_text_new ();
  clutter_text_set_color (CLUTTER_TEXT (sample_item_video->label), &text_color);
  clutter_text_set_font_name (CLUTTER_TEXT (sample_item_video->label), "Sans 28px");
  clutter_actor_set_position (sample_item_video->label, 150, 50);
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item_video), sample_item_video->label);
  clutter_actor_show (sample_item_video->label);

  line = clutter_rectangle_new_with_color (&line_color);
  clutter_actor_set_size (line, 52, 1);
  clutter_actor_set_position (line, 11, 120);
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item_video), line);
  clutter_actor_show (line);

  line = clutter_rectangle_new_with_color (&line_color);
  clutter_actor_set_size (line, 709, 1);
  clutter_actor_set_position (line, 67, 120);
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item_video), line);
  clutter_actor_show (line);
}

ClutterActor*
sample_item_video_new (void)
{
    return g_object_new (TYPE_SAMPLE_ITEM_VIDEO, NULL);
}
