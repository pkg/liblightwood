/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2011 Robert Bosch Car Multimedia GmbH
 * Author: Tomeu Vizoso <tomeu.vizoso@collabora.co.uk>
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef SAMPLE_GLOW_SHADER_H
#define SAMPLE_GLOW_SHADER_H

#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define TYPE_SAMPLE_GLOW_SHADER sample_glow_shader_get_type()

#define SAMPLE_GLOW_SHADER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  TYPE_SAMPLE_GLOW_SHADER, SampleGlowShader))

#define SAMPLE_GLOW_SHADER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  TYPE_SAMPLE_GLOW_SHADER, SampleGlowShaderClass))

#define IS_SAMPLE_GLOW_SHADER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  TYPE_SAMPLE_GLOW_SHADER))

#define IS_SAMPLE_GLOW_SHADER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  TYPE_SAMPLE_GLOW_SHADER))

#define SAMPLE_GLOW_SHADER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  TYPE_SAMPLE_GLOW_SHADER, SampleGlowShaderClass))

typedef struct _SampleGlowShader SampleGlowShader;
typedef struct _SampleGlowShaderClass SampleGlowShaderClass;

struct _SampleGlowShader
{
  ClutterShaderEffect parent;
};

struct _SampleGlowShaderClass
{
  ClutterShaderEffectClass parent_class;
};

GType sample_glow_shader_get_type (void) G_GNUC_CONST;

ClutterEffect *sample_glow_shader_new (void);

G_END_DECLS

#endif /* SAMPLE_GLOW_SHADER_H */
