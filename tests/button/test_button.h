/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* test-liblightwood-button.h */

#ifndef _TEST_BUTTON_H
#define _TEST_BUTTON_H

#include <glib-object.h>
#include "liblightwood-button.h"

G_BEGIN_DECLS

#define TEST_TYPE_BUTTON test_button_get_type()

#define TEST_BUTTON(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  TEST_TYPE_BUTTON, TestButton))

#define TEST_BUTTON_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  TEST_TYPE_BUTTON, TestButtonClass))

#define TEST_IS_BUTTON(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  TEST_TYPE_BUTTON))

#define TEST_IS_BUTTON_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  TEST_TYPE_BUTTON))

#define TEST_BUTTON_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  TEST_TYPE_BUTTON, TestButtonClass))

typedef struct _TestButton TestButton;
typedef struct _TestButtonClass TestButtonClass;
typedef struct _TestButtonPrivate TestButtonPrivate;

struct _TestButton
{
  LightwoodButton parent;

  TestButtonPrivate *priv;
};

struct _TestButtonClass
{
  LightwoodButtonClass parent_class;
};

GType test_button_get_type (void) G_GNUC_CONST;

ClutterActor *test_button_new (void);

G_END_DECLS

#endif /* _TEST_BUTTON_H */
