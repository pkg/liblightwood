/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <clutter/clutter.h>
#include <thornbury/thornbury.h>

#define STATUS_BAR_FILE_NAME    EXAMPLES_DATADIR"/StatusBar_DRAGON_plain.png"
#define DEFAULT_ICON_FILE_NAME  EXAMPLES_DATADIR"/icon_music.png"
#define ICON1_FILE_NAME         EXAMPLES_DATADIR"/icon_email.png"
#define ICON2_FILE_NAME         EXAMPLES_DATADIR"/icon_music.png"
#define VIDEO1_FILE_NAME        EXAMPLES_DATADIR"/01_appl_launcher_2009.03.09.avi"
#define VIDEO2_FILE_NAME        EXAMPLES_DATADIR"/03_content_roll_2009.03.09.avi"
#define COVER1_FILE_NAME        EXAMPLES_DATADIR"/cover1.jpg"
#define COVER2_FILE_NAME        EXAMPLES_DATADIR"/cover2.jpg"
#define COVER3_FILE_NAME        EXAMPLES_DATADIR"/cover3.jpg"
#define COVER4_FILE_NAME        EXAMPLES_DATADIR"/cover4.jpg"
#define THUMB1_FILE_NAME        EXAMPLES_DATADIR"/01_appl_launcher_2009.03.09.png"
#define THUMB2_FILE_NAME        EXAMPLES_DATADIR"/03_content_roll_2009.03.09.png"
#define LONG_TEXT               "long text for item %i\nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \nblah blah blah blah blah blah blah blah \n"

enum
{
  COLUMN_NAME,
  COLUMN_ICON,
  COLUMN_LABEL,
  COLUMN_TOGGLE,
  COLUMN_VIDEO,
  COLUMN_EXTRA_HEIGHT,
  COLUMN_COVER,
  COLUMN_THUMB,
  COLUMN_LONG_TEXT,

  COLUMN_LAST
};

ClutterActor *lightwood_create_status_bar (void);
ThornburyModel *lightwood_create_model (guint num_of_items);
ClutterActor *lightwood_create_scroll_view (void);
