/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "uielements.h"
#include <math.h>
#include <thornbury/thornbury.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop

#define with_cogl 0

ClutterActor *
lightwood_create_status_bar (void)
{
  ClutterActor *status_bar;
  ClutterActor *texture;
  ClutterActor *label;
  ClutterColor white = {255, 255, 255, 255};

  status_bar = clutter_actor_new ();

  texture = thornbury_ui_texture_create_new(STATUS_BAR_FILE_NAME,0,0,FALSE,FALSE);//clutter_texture_new_from_file (STATUS_BAR_FILE_NAME, NULL);
  clutter_actor_set_position (texture, 0, 0);
  clutter_actor_add_child (CLUTTER_ACTOR (status_bar), texture);
  clutter_actor_set_reactive (texture, TRUE);
  clutter_actor_show (texture);

  texture = thornbury_ui_texture_create_new(DEFAULT_ICON_FILE_NAME,0,0,FALSE,FALSE);//clutter_texture_new_from_file (DEFAULT_ICON_FILE_NAME, NULL);
  clutter_actor_set_position (texture, 15, 7);
  clutter_actor_add_child (CLUTTER_ACTOR (status_bar), texture);
  clutter_actor_show (texture);

  label = clutter_text_new_full ("Sans 24px", "MultiMedia Demonstrator", &white);
  clutter_actor_add_child (CLUTTER_ACTOR (status_bar), label);
  clutter_actor_set_position (label, 76, 16);
  clutter_actor_set_width (label, 500);
  clutter_text_set_ellipsize (CLUTTER_TEXT (label), PANGO_ELLIPSIZE_END);
  clutter_actor_show (label);

  return status_bar;
}

ThornburyModel *
lightwood_create_model (guint num_of_items)
{
#if with_cogl
	CoglHandle icon1, icon2, thumb1, thumb2;
	CoglHandle covers[4];
#else
  const gchar *icon1, *icon2;
  gchar *covers[4];
#endif
	ThornburyModel *model;
        guint i;

#if with_cogl
	icon1 = cogl_texture_new_from_file (ICON1_FILE_NAME, COGL_TEXTURE_NONE,
			COGL_PIXEL_FORMAT_ANY, NULL);

	icon2 = cogl_texture_new_from_file (ICON2_FILE_NAME, COGL_TEXTURE_NONE,
			COGL_PIXEL_FORMAT_ANY, NULL);
#else
	icon1 = ICON1_FILE_NAME;
	icon2 = ICON2_FILE_NAME;
#endif

#if with_cogl 
	covers[0] = cogl_texture_new_from_file (COVER1_FILE_NAME, COGL_TEXTURE_NONE,
			COGL_PIXEL_FORMAT_ANY, NULL);

	covers[1] = cogl_texture_new_from_file (COVER2_FILE_NAME, COGL_TEXTURE_NONE,
			COGL_PIXEL_FORMAT_ANY, NULL);

	covers[2] = cogl_texture_new_from_file (COVER3_FILE_NAME, COGL_TEXTURE_NONE,
			COGL_PIXEL_FORMAT_ANY, NULL);

	covers[3] = cogl_texture_new_from_file (COVER4_FILE_NAME, COGL_TEXTURE_NONE,
			COGL_PIXEL_FORMAT_ANY, NULL);
#else
	covers[0] = g_strdup(COVER1_FILE_NAME);

	covers[1] = g_strdup(COVER2_FILE_NAME);

	covers[2] = g_strdup(COVER3_FILE_NAME);

	covers[3] = g_strdup(COVER4_FILE_NAME);
#endif

#if with_cogl
	thumb1 = cogl_texture_new_from_file (THUMB1_FILE_NAME, COGL_TEXTURE_NONE,
			COGL_PIXEL_FORMAT_ANY, NULL);

	thumb2 = cogl_texture_new_from_file (THUMB2_FILE_NAME, COGL_TEXTURE_NONE,
			COGL_PIXEL_FORMAT_ANY, NULL);
#endif


	model = (ThornburyModel *)thornbury_list_model_new (COLUMN_LAST, G_TYPE_STRING, NULL,
#if with_cogl
			COGL_TYPE_HANDLE, NULL,
#else
			G_TYPE_STRING, NULL,
#endif
			G_TYPE_STRING, NULL,
			G_TYPE_BOOLEAN, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_FLOAT, NULL,
#if with_cogl
			COGL_TYPE_HANDLE, NULL,
			COGL_TYPE_HANDLE, NULL,
#else
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
#endif
			G_TYPE_STRING, NULL,
			-1);

	for (i = 0; i < num_of_items; i++)
		thornbury_model_append (model, COLUMN_NAME, g_strdup_printf ("item number %i", i),
				COLUMN_ICON, i % 2 == 0 ? icon1 : icon2,
				COLUMN_LABEL, g_strdup_printf ("item number %i", i),
				COLUMN_TOGGLE, i % 2,
				COLUMN_VIDEO, i % 2 == 0 ? VIDEO1_FILE_NAME : VIDEO2_FILE_NAME,
				COLUMN_EXTRA_HEIGHT, (float) (i % 100),
				COLUMN_COVER, covers[i % 3],
#if with_cogl
				COLUMN_THUMB, i % 2 == 0 ? thumb1 : thumb2,
#else
				COLUMN_THUMB,NULL,
#endif
				COLUMN_LONG_TEXT, g_strdup_printf (LONG_TEXT, i),
				-1);

	return model;
}

ClutterActor *
lightwood_create_scroll_view (void)
{
  return g_object_new (MX_TYPE_KINETIC_SCROLL_VIEW,
                       "acceleration-factor", 2.0,
                       "deceleration", 1.03,
                       "clamp-duration", 1250,
                       "clamp-mode", CLUTTER_EASE_OUT_ELASTIC,
                       "clamp-to-center", TRUE,
                       "overshoot", 1.0,
                       NULL);
}
