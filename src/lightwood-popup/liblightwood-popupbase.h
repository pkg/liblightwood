/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-popupbase.h */

#ifndef __LIGHTWOOD_POPUP_BASE_H__
#define __LIGHTWOOD_POPUP_BASE_H__

#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_POPUP_BASE lightwood_popup_base_get_type ()
G_DECLARE_DERIVABLE_TYPE (LightwoodPopupBase, lightwood_popup_base, LIGHTWOOD, POPUP_BASE, ClutterActor)

struct _LightwoodPopupBaseClass
{
    ClutterActorClass parent_class;

    void (* popup_shown)  (LightwoodPopupBase *self);
    void (* popup_hidden) (LightwoodPopupBase *self);
    void (* popup_action) (LightwoodPopupBase *self,
                           const gchar *button,
                           gpointer pValue);
};

gdouble lightwood_popup_base_get_timeout (LightwoodPopupBase *self);
void lightwood_popup_base_set_timeout (LightwoodPopupBase *self,
                                       gdouble timeout);

const gchar * lightwood_popup_base_get_sound_data (LightwoodPopupBase *self);
void lightwood_popup_base_set_sound_data (LightwoodPopupBase *self,
                                          const gchar *sound);

G_END_DECLS

#endif /* __POPUP_BASE_H__ */
