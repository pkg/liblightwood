/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:liblightwood-mapbase
 * @Title:LightwoodMap
 * @Short_Description: Base Map widget uses OSM(Open Street Map) as backend.
 * @See_Also: #ClutterActor
 *
 * #LightwoodMap is a abstract class which has basic properties and signals.Given a lattitude and logtitude 
 * it can plot in to the openstreen map. It is using OSM(Open Street Map) as a backend. This widget 
 * can be extended as given below.
 * 
 * 
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 *
 * G_DEFINE_TYPE( MapWidget, map_widget, MAP_TYPE_BASE)
 *
 *#define WIDGET_PRIVATE(o) \
 * (G_TYPE_INSTANCE_GET_PRIVATE ((o), MAP_TYPE_WIDGET, MapWidgetPrivate))
 * // code goes here..
 * ]|
 *
 */ 

/*******************************************************************************
 * @Project Includes
 ******************************************************************************/

#include "liblightwood-mapbase.h"
#include <thornbury/thornbury.h>

typedef enum _MapProperties MapProperties;
typedef enum _MapSignals MapSignals;

/**
 * _MapProperties:
 *
 * property enums of Map widget
 */
enum _MapProperties
{
/*< private >*/
    MAP_PROP_0,
    MAP_PROP_ZOOM_LEVEL,
    MAP_PROP_LATITUDE,
    MAP_PROP_LONGITUDE,
    MAP_PROP_BACKGROUND


};

enum _MapSignals
{
    /* normal signals */
    UPDATE_COMPLETE, THUMBNAIL_COMPLETE, MARKER_SELECTED, LAST_SIGNAL
};

static guint uinMapSignals[LAST_SIGNAL] = { 0, };

typedef struct
{
/*< private >*/
    gfloat longitude;
    gfloat latitude;
    gint currentZoomLevel;
    ClutterActor *content_map;
    ClutterActor *content_bkground;
    ClutterActor *content_group;
    ClutterActor *pLayerActor;
    ClutterActor *pPathLayerActor;
    ClutterActor *pPosDestinationLayer;
    gchar *pImagepath;

} LightwoodMapBasePrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (LightwoodMapBase, lightwood_map_base, CLUTTER_TYPE_ACTOR)


/*************************************************************************************
 * lightwood_map_base_animate_actor
 * @pActor	: Actor to be animated.
 * @ineaseType	: Clutter Actor enum for animation.%ClutterAnimationMode
 * @inTime	: inmsec
 * @inWidth	: width
 * @inHeight	: height
 * this function is used to animate the map actor
 *
 **************************************************************************************/

void
lightwood_map_base_animate_actor (LightwoodMapBase *self,
                                  ClutterActor *pActor,
                                  gint ineaseType,
                                  gint inTime,
                                  gint inWidth,
                                  gint inHeight)
{
    g_return_if_fail (LIGHTWOOD_IS_MAP_BASE (self));

    clutter_actor_save_easing_state(pActor);
    clutter_actor_set_easing_mode(pActor, ineaseType);
    clutter_actor_set_easing_duration(pActor, inTime);
    clutter_actor_set_width(pActor, inWidth);
    clutter_actor_set_height(pActor, inHeight);
    clutter_actor_restore_easing_state(pActor);

}

/**
 * lightwood_map_base_set_latitude:
 * @self: a #LightwoodButton
 * @latitude: the new latitude
 *
 * Update the #LightwoodMapBase:latitude property
 */
void
lightwood_map_base_set_latitude (LightwoodMapBase *self,
                                 gfloat latitude)
{
    LightwoodMapBasePrivate *priv = lightwood_map_base_get_instance_private (self);

    g_return_if_fail (LIGHTWOOD_MAP_BASE (self));

    priv->latitude = latitude;
    g_object_notify (G_OBJECT (self), "latitude");
}

/**
 * lightwood_map_base_set_longitude:
 * @self: a #LightwoodButton
 * @longitude: the new longitude
 *
 * Update the #LightwoodMapBase:longitude property
 */
void
lightwood_map_base_set_longitude (LightwoodMapBase *self,
                                  gfloat longitude)
{
    LightwoodMapBasePrivate *priv = lightwood_map_base_get_instance_private (self);

    g_return_if_fail (LIGHTWOOD_MAP_BASE (self));

    priv->longitude = longitude;
    g_object_notify (G_OBJECT (self), "longitude");
}

/**
 * lightwood_map_base_set_zoom_level:
 * @self: a #LightwoodButton
 * @zoom: the new zoom value
 *
 * Update the #LightwoodMapBase:zoom-level property
 */
void
lightwood_map_base_set_zoom_level (LightwoodMapBase *self,
                                   guint zoom)
{
    LightwoodMapBasePrivate *priv = lightwood_map_base_get_instance_private (self);

    g_return_if_fail (LIGHTWOOD_MAP_BASE (self));
    g_return_if_fail (zoom >=2 && zoom <= 18);

    priv->currentZoomLevel = zoom;
    champlain_view_set_zoom_level(CHAMPLAIN_VIEW(priv->content_map),
                                  priv->currentZoomLevel);

    g_object_notify (G_OBJECT (self), "zoom-level");
    g_signal_emit_by_name (self, "update-complete", priv->currentZoomLevel);
}

/**
 * lightwood_map_base_set_background:
 * @self: a #LightwoodButton
 * @path: a file path
 *
 * Update the #LightwoodMapBase:background property
 */
void
lightwood_map_base_set_background (LightwoodMapBase *self,
                                   const gchar *path)
{
    LightwoodMapBasePrivate *priv = lightwood_map_base_get_instance_private (self);

    g_return_if_fail (LIGHTWOOD_MAP_BASE (self));

    g_free (priv->pImagepath);
    priv->pImagepath = g_strdup (path);

    if(NULL == priv->content_bkground)
    {
        priv->content_bkground = thornbury_ui_texture_create_new(priv->pImagepath, 0, 0,
                                        FALSE, FALSE);
        clutter_actor_set_name(priv->content_bkground, "MapBKGround");
        clutter_actor_add_child (CLUTTER_ACTOR (self), priv->content_bkground);
        clutter_actor_set_child_at_index (CLUTTER_ACTOR (self),priv->content_bkground,0);
    }
    else
    {
        thornbury_ui_texture_set_from_file(priv->content_bkground,priv->pImagepath,clutter_actor_get_width(priv->content_bkground),clutter_actor_get_height(priv->content_bkground),FALSE,FALSE);
    }

    g_object_notify (G_OBJECT (self), "background");
}




static void v_map_base_zoom_level_change_clb(ClutterActor *actor,gint zoom_level,gpointer data)
{
	LightwoodMapBase *pSelf = LIGHTWOOD_MAP_BASE(data);
   LightwoodMapBasePrivate *priv = lightwood_map_base_get_instance_private (pSelf);
   priv->currentZoomLevel = champlain_view_get_zoom_level(CHAMPLAIN_VIEW(priv->content_map));
   g_signal_emit_by_name(pSelf, "update-complete",
                                 priv->currentZoomLevel);

}

/******************************************************************************
 * function		: v_map_base_get_property
 * PARAMETER	: GObject *,guint,const GValue*,GParamSpec *.
 * Returns	: void
 * description	: this function is called when ever g_object_get is called on the object
 * AUTHOR		: Rohan Madgula
 *
 *******************************************************************************/

static void
v_map_base_get_property (GObject    *pObject,
                         guint       property_id,
                         GValue     *pValue,
                         GParamSpec *pPspec)
{
    LightwoodMapBase *pSelf = LIGHTWOOD_MAP_BASE(pObject);
    LightwoodMapBasePrivate *priv = lightwood_map_base_get_instance_private (pSelf);

    switch (property_id)
    {
    case MAP_PROP_ZOOM_LEVEL:
        g_value_set_uint (pValue, priv->currentZoomLevel);
        break;

    case MAP_PROP_LATITUDE:
        g_value_set_float(pValue, priv->latitude);
        break;
    case MAP_PROP_LONGITUDE:
        g_value_set_float(pValue, priv->longitude);
        break;
    case MAP_PROP_BACKGROUND:
        g_value_set_string(pValue,priv->pImagepath);
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, property_id, pPspec);
    }
}


/******************************************************************************
 * function		: v_map_base_set_property
 * PARAMETER	: GObject *,guint,const GValue*,GParamSpec *.
 * Returns	: void
 * description	: this function is called when ever g_object_set is called on the object
 * AUTHOR		: Rohan Madgula
 *
 *******************************************************************************/
static void
v_map_base_set_property (GObject      *pObject,
                         guint         property_id,
                         const GValue *pValue,
                         GParamSpec   *pPspec)
{
    LightwoodMapBase *self = LIGHTWOOD_MAP_BASE (pObject);

    switch (property_id)
    {
    case MAP_PROP_ZOOM_LEVEL:
        lightwood_map_base_set_zoom_level (self, g_value_get_uint (pValue));
        break;

    case MAP_PROP_LATITUDE:
        lightwood_map_base_set_latitude (self, g_value_get_float (pValue));
        break;
    case MAP_PROP_LONGITUDE:
        lightwood_map_base_set_longitude (self, g_value_get_float (pValue));
        break;
    case MAP_PROP_BACKGROUND:
        lightwood_map_base_set_background (self, g_value_get_string (pValue));
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, property_id, pPspec);
    }
}

/******************************************************************************
 * function		: v_map_base_dispose
 * PARAMETER	: GObject *.
 * Returns	: void
 * description	: this function is called just before  object is freed
 * AUTHOR		: Rohan Madgula
 *
 *******************************************************************************/
static void
v_map_base_dispose (GObject *object)
{
    G_OBJECT_CLASS (lightwood_map_base_parent_class)->dispose (object);
}

/******************************************************************************
 * function		: v_map_base_finalize
 * PARAMETER	: GObject *.
 * Returns	: void
 * description	: this function is called just before  object is freed
 * AUTHOR		: Rohan Madgula
 *
 *******************************************************************************/

static void
v_map_base_finalize (GObject *object)
{
    LightwoodMapBase *self = LIGHTWOOD_MAP_BASE (object);
    LightwoodMapBasePrivate *priv = lightwood_map_base_get_instance_private (self);

    g_free (priv->pImagepath);

    G_OBJECT_CLASS (lightwood_map_base_parent_class)->finalize (object);
}

/******************************************************************************
 * function		: map_base_class_init
 * PARAMETER	: The object's class reference
 * Returns	: void
 * description	: Class initialisation function for the object type.
 *               Called automatically on the first call to g_object_new
 * AUTHOR		: Rohan Madgula
 *
 *******************************************************************************/

static void
lightwood_map_base_class_init (LightwoodMapBaseClass *klass)
{
    GObjectClass *pObjectClass = G_OBJECT_CLASS (klass);
    GParamSpec *pPspec = NULL;

    pObjectClass->get_property = v_map_base_get_property;
    pObjectClass->set_property = v_map_base_set_property;
    pObjectClass->dispose = v_map_base_dispose;
    pObjectClass->finalize = v_map_base_finalize;
    /**
                     * LightwoodMapBase:zoom-level:
                     *
                     * current zoom level of the map widget
                     * DEFAULT:15
                     */
    pPspec = g_param_spec_uint("zoom-level", "zoom-level",
                              "Zoom level of the MAP", 2, 18, 15, G_PARAM_READWRITE);
    g_object_class_install_property(pObjectClass, MAP_PROP_ZOOM_LEVEL,
                                    pPspec);
    /**
                             * LightwoodMapBase:latitude:
                             *
                             * latitude for map widget to centre on
                             * DEFAULT:0.0
                             */
    pPspec = g_param_spec_float("latitude", "latitude",
                                "latitude of the location in MAP widget", -180.0, 180.0, 0.0,
                                G_PARAM_READWRITE);
    g_object_class_install_property(pObjectClass, MAP_PROP_LATITUDE, pPspec);

    /**
                                 * LightwoodMapBase:longitude:
                                 *
                                 * longitude for map widget to centre on
                                 * DEFAULT:0.0
                                 */
    pPspec = g_param_spec_float("longitude", "longitude",
                                "longitude of the location in MAP widget", -180.0, 180.0, 0.0,
                                G_PARAM_READWRITE);
    g_object_class_install_property(pObjectClass, MAP_PROP_LONGITUDE, pPspec);


    pPspec = g_param_spec_string ("background", "background",
                                 "set the image path of background image  in MAP widget",NULL,
                                 G_PARAM_READWRITE);
    g_object_class_install_property(pObjectClass, MAP_PROP_BACKGROUND, pPspec);

	 /**
          * LightwoodMapBase::update-complete:
          * @map: the #LightwoodMapBase emitting this signal
          * @zoom: the current zoom level
          *
          * #LightwoodMapBase::update-complete is emitted when flip animation
          * begins for the visible first actor
          */

    uinMapSignals[UPDATE_COMPLETE] = g_signal_new("update-complete",
                                     G_OBJECT_CLASS_TYPE(pObjectClass),
                                     G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST, 0, NULL, NULL,
                                     g_cclosure_marshal_VOID__INT, G_TYPE_NONE, 1, G_TYPE_INT);

    /**
     * LightwoodMapBase::thumbnail-complete:
     * @map: the #LightwoodMapBase emitting this signal
     * @thumbnail: the thumbnail as a #ClutterActor
     *
     * "thumbnail-complete" signal will be emitted when ever the thumbnail capture
     * is completed
     * Since: 0.10
     **/
    uinMapSignals[THUMBNAIL_COMPLETE] = g_signal_new("thumbnail-complete",
                                        G_OBJECT_CLASS_TYPE(pObjectClass),
                                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST, 0, NULL, NULL,
                                        g_cclosure_marshal_VOID__OBJECT, G_TYPE_NONE, 1, CLUTTER_TYPE_ACTOR);

    /**
     * LightwoodMapBase::marker-selected:
     * @map: the #LightwoodMapBase emitting this signal
     * @marker: the name (as returned by clutter_actor_get_name()) of the selected marker
     *
     * "marker-selected" signal will be emitted when ever the corresponding
     *  marker is clicked.
     *Since: 0.10
     **/

    uinMapSignals[MARKER_SELECTED] = g_signal_new("marker-selected",
                                     G_OBJECT_CLASS_TYPE(pObjectClass),
                                     G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST, 0, NULL, NULL,
                                     g_cclosure_marshal_VOID__STRING, G_TYPE_NONE, 1, G_TYPE_STRING);


}

/******************************************************************************
 * function		: map_base_init
 * PARAMETER	: The object's reference
 * Returns	: void
 * description	: initialisation function for the object type.
 *                Called automatically on every call to g_object_new
 * AUTHOR		: Rohan Madgula
 *
 *******************************************************************************/

#define DEFAULT_WIDTH 728
#define DEFAULT_HEIGHT 480

static void
lightwood_map_base_init (LightwoodMapBase *pSelf)
{
    LightwoodMapBasePrivate *priv = lightwood_map_base_get_instance_private (pSelf);

    priv->latitude = 0.0;
    priv->longitude = 0.0;
    priv->currentZoomLevel = 15;
    priv->content_map = NULL;
    priv->content_bkground = NULL;
    priv->content_group = NULL;
    priv->pImagepath = NULL;
    priv->content_map = champlain_view_new();
    priv->pPathLayerActor = CLUTTER_ACTOR(champlain_path_layer_new());
    priv->pLayerActor = CLUTTER_ACTOR(
                                   champlain_marker_layer_new_full(CHAMPLAIN_SELECTION_SINGLE));
    priv->pPosDestinationLayer = CLUTTER_ACTOR(
                                   champlain_marker_layer_new_full(CHAMPLAIN_SELECTION_SINGLE));
    clutter_actor_add_child(CLUTTER_ACTOR(pSelf), priv->content_map);
    champlain_view_set_zoom_on_double_click(
        CHAMPLAIN_VIEW(priv->content_map), FALSE);
    g_signal_connect(priv->content_map,"notify::zoom-level",G_CALLBACK(v_map_base_zoom_level_change_clb),pSelf);
    champlain_view_set_min_zoom_level(CHAMPLAIN_VIEW(priv->content_map),
                                      2);
    clutter_actor_set_reactive(priv->content_map, TRUE);
    g_object_set(G_OBJECT(priv->content_map), "zoom-level",
                 priv->currentZoomLevel, "kinetic-mode", TRUE, NULL);
    champlain_view_center_on(CHAMPLAIN_VIEW(priv->content_map),
                             priv->latitude, priv->longitude);


    clutter_actor_set_size(priv->content_map, DEFAULT_WIDTH, DEFAULT_HEIGHT);
 champlain_view_add_layer(CHAMPLAIN_VIEW(priv->content_map),
                             CHAMPLAIN_LAYER(priv->pPathLayerActor));
    champlain_view_add_layer(CHAMPLAIN_VIEW(priv->content_map),
                             CHAMPLAIN_LAYER(priv->pLayerActor));
    champlain_view_add_layer(CHAMPLAIN_VIEW(priv->content_map),
                             CHAMPLAIN_LAYER(priv->pPosDestinationLayer));




}

/**
 * lightwood_map_base_get_map:
 * @self: a #LightwoodMapBase
 *
 * this function is used to return the map actor
 *
 * Returns: (transfer none): a #ClutterActor
 */
ClutterActor *
lightwood_map_base_get_map (LightwoodMapBase *self)
{
  LightwoodMapBasePrivate *priv = lightwood_map_base_get_instance_private (self);

  g_return_val_if_fail (LIGHTWOOD_IS_MAP_BASE (self), NULL);

  return priv->content_map;
}


/**
 * lightwood_map_base_get_map_background:
 * @self: a #LightwoodMapBase
 *
 * this function is used to return the map background actor
 *
 * Returns: (transfer none) (nullable): a #ClutterActor
 */
ClutterActor *
lightwood_map_base_get_map_background (LightwoodMapBase *self)
{
  LightwoodMapBasePrivate *priv = lightwood_map_base_get_instance_private (self);

  g_return_val_if_fail (LIGHTWOOD_IS_MAP_BASE (self), NULL);

  return priv->content_bkground;
}

/**
 * lightwood_map_base_get_layer_actor:
 * @self: a #LightwoodMapBase
 *
 * this function is used to return layer actor
 *
 * Returns: (transfer none): a #ClutterActor
 */
ClutterActor *
lightwood_map_base_get_layer_actor (LightwoodMapBase *self)
{
  LightwoodMapBasePrivate *priv = lightwood_map_base_get_instance_private (self);

  g_return_val_if_fail (LIGHTWOOD_IS_MAP_BASE (self), NULL);

  return priv->pLayerActor;
}

/**
 * lightwood_map_base_get_path_layer_actor:
 * @self: a #LightwoodMapBase
 *
 * this function is used to return path layer actor
 *
 * Returns: (transfer none): a #ClutterActor
 */
ClutterActor *
lightwood_map_base_get_path_layer_actor (LightwoodMapBase *self)
{
  LightwoodMapBasePrivate *priv = lightwood_map_base_get_instance_private (self);

  g_return_val_if_fail (LIGHTWOOD_IS_MAP_BASE (self), NULL);

  return priv->pPathLayerActor;
}

/**
 * lightwood_map_base_get_pos_destination_layer:
 * @self: a #LightwoodMapBase
 *
 * this function is used to return position destination layer
 *
 * Returns: (transfer none): a #ClutterActor
 */
ClutterActor *
lightwood_map_base_get_pos_destination_layer (LightwoodMapBase *self)
{
  LightwoodMapBasePrivate *priv = lightwood_map_base_get_instance_private (self);

  g_return_val_if_fail (LIGHTWOOD_IS_MAP_BASE (self), NULL);

  return priv->pPosDestinationLayer;
}

/**
 * lightwood_map_base_get_zoom_level:
 * @self: a #LightwoodMapBase
 *
 * Return the #LightwoodMapBase:zoom-level property
 *
 * Returns: the value of #LightwoodMapBase:zoom-level property
 */
guint
lightwood_map_base_get_zoom_level (LightwoodMapBase *self)
{
  LightwoodMapBasePrivate *priv = lightwood_map_base_get_instance_private (self);

  g_return_val_if_fail (LIGHTWOOD_IS_MAP_BASE (self), 2);

  return priv->currentZoomLevel;
}

/**
 * lightwood_map_base_get_latitude:
 * @self: a #LightwoodMapBase
 *
 * Return the #LightwoodMapBase:latitude property
 *
 * Returns: the value of #LightwoodMapBase:latitude property
 */
gfloat
lightwood_map_base_get_latitude (LightwoodMapBase *self)
{
  LightwoodMapBasePrivate *priv = lightwood_map_base_get_instance_private (self);

  g_return_val_if_fail (LIGHTWOOD_IS_MAP_BASE (self), 0.0);

  return priv->latitude;
}

/**
 * lightwood_map_base_get_longitude:
 * @self: a #LightwoodMapBase
 *
 * Return the #LightwoodMapBase:longitude property
 *
 * Returns: the value of #LightwoodMapBase:longitude property
 */
gfloat
lightwood_map_base_get_longitude (LightwoodMapBase *self)
{
  LightwoodMapBasePrivate *priv = lightwood_map_base_get_instance_private (self);

  g_return_val_if_fail (LIGHTWOOD_IS_MAP_BASE (self), 0.0);

  return priv->longitude;
}

/**
 * lightwood_map_base_get_background:
 * @self: a #LightwoodMapBase
 *
 * Return the #LightwoodMapBase:background property
 *
 * Returns: the value of #LightwoodMapBase:background property
 */
const gchar *
lightwood_map_base_get_background (LightwoodMapBase *self)
{
  LightwoodMapBasePrivate *priv = lightwood_map_base_get_instance_private (self);

  g_return_val_if_fail (LIGHTWOOD_IS_MAP_BASE (self), NULL);

  return priv->pImagepath;
}
