/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-mapbase.h */

#ifndef __LIGHTWOOD_MAP_BASE_H__
#define __LIGHTWOOD_MAP_BASE_H__

#include <glib-object.h>
#include <clutter/clutter.h>
#include <champlain/champlain.h>

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_MAP_BASE lightwood_map_base_get_type ()
G_DECLARE_DERIVABLE_TYPE (LightwoodMapBase, lightwood_map_base, LIGHTWOOD, MAP_BASE, ClutterActor)

struct _LightwoodMapBaseClass
{
  ClutterActorClass parent_class;
};

ClutterActor *lightwood_map_base_get_map (LightwoodMapBase *self);
ClutterActor *lightwood_map_base_get_map_background (LightwoodMapBase *self);
ClutterActor *lightwood_map_base_get_layer_actor (LightwoodMapBase *self);
ClutterActor *lightwood_map_base_get_path_layer_actor (LightwoodMapBase *self);
ClutterActor *lightwood_map_base_get_pos_destination_layer (LightwoodMapBase *self);

void lightwood_map_base_animate_actor (LightwoodMapBase *self,
                                       ClutterActor *pActor,
                                       gint ineaseType,
                                       gint inTime,
                                       gint inWidth,
                                       gint inHeight);

guint lightwood_map_base_get_zoom_level (LightwoodMapBase *self);
void lightwood_map_base_set_zoom_level (LightwoodMapBase *self,
                                        guint zoom);

gfloat lightwood_map_base_get_latitude (LightwoodMapBase *self);
void lightwood_map_base_set_latitude (LightwoodMapBase *self,
                                      gfloat latitude);
gfloat lightwood_map_base_get_longitude (LightwoodMapBase *self);
void lightwood_map_base_set_longitude (LightwoodMapBase *self,
                                       gfloat longitude);

const gchar *lightwood_map_base_get_background (LightwoodMapBase *self);
void lightwood_map_base_set_background (LightwoodMapBase *self,
                                        const gchar *path);

G_END_DECLS

#endif /* __LIGHTWOOD_MAP_BASE_H__ */
