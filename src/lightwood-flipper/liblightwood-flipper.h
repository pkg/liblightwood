/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _LIGHTWOOD_FLIPPER_H
#define _LIGHTWOOD_FLIPPER_H

/*******************************************************************************
 * @System Includes
 ******************************************************************************/
#include <glib-object.h>
#include <clutter/clutter.h>

/*******************************************************************************
 * @Project Includes
 ******************************************************************************/
#include "lightwood-enumtypes.h"
#include <thornbury/thornbury.h>

/******************************************************************************
 * @Defines
 *****************************************************************************/
G_BEGIN_DECLS

/**
 * LIGHTWOOD_TYPE_FLIPPER:
 *
 * Get the #GType of the #LightwoodFlipper
 */
#define LIGHTWOOD_TYPE_FLIPPER lightwood_flipper_get_type()

/**
 * LIGHTWOOD_FLIPPER:
 * @obj: Valid #GObject
 *
 * Get the #LightwoodFlipper object.
 */
#define LIGHTWOOD_FLIPPER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LIGHTWOOD_TYPE_FLIPPER, LightwoodFlipper))

#define LIGHTWOOD_FLIPPER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LIGHTWOOD_TYPE_FLIPPER, LightwoodFlipperClass))

#define LIGHTWOOD_IS_FLIPPER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LIGHTWOOD_TYPE_FLIPPER))

#define LIGHTWOOD_IS_FLIPPER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LIGHTWOOD_TYPE_FLIPPER))

#define LIGHTWOOD_FLIPPER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LIGHTWOOD_TYPE_FLIPPER, LightwoodFlipperClass))

typedef struct _LightwoodFlipper LightwoodFlipper;
typedef struct _LightwoodFlipperClass LightwoodFlipperClass;
typedef struct _LightwoodFlipperPrivate LightwoodFlipperPrivate;



/**
 * enLightwoodFlipperModel:
 * @LIGHTWOOD_FLIPPER_COLUMN_FIRST_ACTOR: Model column One data should contain the Front object pointer.
 * @LIGHTWOOD_FLIPPER_COLUMN_SECOND_ACTOR: Model column One data should contain the Back object pointer.
 * @LIGHTWOOD_FLIPPER_COLUUMN_LAST: Represents total no of columns.
 *
 * Model format to flip two objects using #LightwoodFlipper widget.
 *
 */
typedef enum {
    LIGHTWOOD_FLIPPER_COLUMN_FIRST_ACTOR,
    LIGHTWOOD_FLIPPER_COLUMN_SECOND_ACTOR,
    LIGHTWOOD_FLIPPER_COLUUMN_LAST
} LightwoodFlipperModel;

/**
 * LightwoodFlipCycle:
 * @LIGHTWOOD_ENABLE_FULL_FLIP_CYCLE: Flip both objects provided model data contains valid object pair.
 * @LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY: Flip only first actor in model provided it is a Non-Null Valid object.
 * @LIGHTWOOD_ENABLE_SECOND_ACTOR_FLIP_ONLY: Flip only second actor in model provided it is a Non-Null Valid object.
 *
 * Enum to define the flip cycle for two objects using #LightwoodFlipper widget.
 *
 */
typedef enum {
	LIGHTWOOD_ENABLE_FULL_FLIP_CYCLE,
	LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY,
	LIGHTWOOD_ENABLE_SECOND_ACTOR_FLIP_ONLY
} LightwoodFlipCycle;


/*< public >*/

/**
* LightwoodFlipper:
* @parent: Parent of #LightwoodFlipper
*
* Base class of #LightwoodFlipper widget .
*
*/
struct _LightwoodFlipper
{
  ClutterActor parent;

  LightwoodFlipperPrivate *priv;
};

/**
* LightwoodFlipperClass:
* @flip_animation_half_done: Signal class closure for #LightwoodFlipper::flip-animation-half-mark.
* @flip_animation_completed: Signal class closure for #LightwoodFlipper::flip-animation-completed.
* @flip_animation_started: Signal class closure for #LightwoodFlipper::flip-animation-started.
* @flip_animation_reversed: Signal class closure for #LightwoodFlipper::flip-animation-reversed.
* @flip_animation_mark_reached: Signal class closure for #LightwoodFlipper::flip-animation-mark-reached.
*
*
* Base class of #LightwoodFlipper widget .
*
*/

struct _LightwoodFlipperClass
{
  /*< private >*/
  ClutterActorClass parent_class;

  /*< public >*/
  /* Signal format for flip animation half mark reached */
  void (*flip_animation_half_done)    (LightwoodFlipper *self,
                                       ClutterTimelineDirection completed_flip_direction);
  /* Signal format for flip animation completed */
  void (*flip_animation_completed)    (LightwoodFlipper *self,
                                       ClutterActor *actor_in_focus);
  /* Signal format for flip animation started */
  void (*flip_animation_started)      (LightwoodFlipper *self,
                                       ClutterActor *actor_being_animated);
  /* Signal format for flip animation reversed */
  void (*flip_animation_reversed)     (LightwoodFlipper *self,
                                       ClutterActor *actor_in_focus,
                                       ClutterTimelineDirection flip_direction);
  /* signal format for flip animation marker reached */
  void (*flip_animation_mark_reached) (LightwoodFlipper *self,
                                       ClutterActor *actor_being_animated,
                                       ClutterTimelineDirection flip_direction);
};



GType lightwood_flipper_get_type (void) G_GNUC_CONST;
/********************************************************************************
 * @Prototypes of global functions
 *******************************************************************************/
LightwoodFlipper *lightwood_flipper_new (void);

void lightwood_flipper_rewind_animation (LightwoodFlipper *pLightwoodFlipper);
void lightwood_flipper_reverse_animation (LightwoodFlipper *pLightwoodFlipper);
void lightwood_flipper_start_animation (LightwoodFlipper *pLightwoodFlipper);
void lightwood_flipper_stop_animation (LightwoodFlipper *pLightwoodFlipper);
void lightwood_flipper_restart_animation (LightwoodFlipper *pLightwoodFlipper);
void lightwood_flipper_proceed_flip_anim (LightwoodFlipper *pLightwoodFlipper,
                                          ClutterTimelineDirection uinFlipDirection);

/* Properties */
guint lightwood_flipper_get_transition_time (LightwoodFlipper *self);
void lightwood_flipper_set_transition_time (LightwoodFlipper *pLightwoodFlipper,
                                            guint uinTransitionTime);

ClutterAnimationMode lightwood_flipper_get_animation_mode (LightwoodFlipper *self);
void lightwood_flipper_set_animation_mode (LightwoodFlipper *pLightwoodFlipper,
                                           ClutterAnimationMode uinTransitionMode);

ClutterTimelineDirection lightwood_flipper_get_animation_direction (LightwoodFlipper *self);
void lightwood_flipper_set_animation_direction (LightwoodFlipper *pLightwoodFlipper,
                                                ClutterTimelineDirection uinFlipDirection);

ThornburyModel * lightwood_flipper_get_model (LightwoodFlipper *self);
void lightwood_flipper_set_model (LightwoodFlipper *pLightwoodFlipper , ThornburyModel *pModel);

ClutterTimelineDirection lightwood_flipper_get_flip_direction (LightwoodFlipper *self);
void lightwood_flipper_set_flip_direction (LightwoodFlipper *pLightwoodFlipper,
                                           ClutterTimelineDirection uinFlipDirection);

ClutterTimelineDirection lightwood_flipper_get_transition_direction (LightwoodFlipper *self);
void lightwood_flipper_set_transition_direction (LightwoodFlipper *pLightwoodFlipper,
                                           ClutterTimelineDirection uinFlipDirection);

LightwoodFlipCycle lightwood_flipper_get_flip_cycle (LightwoodFlipper *self);
void lightwood_flipper_set_flip_cycle(LightwoodFlipper *pLightwoodFlipper , LightwoodFlipCycle uinFlipCycle);

gdouble lightwood_flipper_get_scale_x_min (LightwoodFlipper *self);
void lightwood_flipper_set_scale_x_min (LightwoodFlipper *self,
                                        gdouble scale);

gdouble lightwood_flipper_get_scale_x_max (LightwoodFlipper *self);
void lightwood_flipper_set_scale_x_max (LightwoodFlipper *self,
                                        gdouble scale);

gdouble lightwood_flipper_get_scale_y_min (LightwoodFlipper *self);
void lightwood_flipper_set_scale_y_min (LightwoodFlipper *self,
                                        gdouble scale);

gdouble lightwood_flipper_get_scale_y_max (LightwoodFlipper *self);
void lightwood_flipper_set_scale_y_max (LightwoodFlipper *self,
                                        gdouble scale);

gdouble lightwood_flipper_get_scale_z_min (LightwoodFlipper *self);
void lightwood_flipper_set_scale_z_min (LightwoodFlipper *self,
                                        gdouble scale);

gdouble lightwood_flipper_get_scale_z_max (LightwoodFlipper *self);
void lightwood_flipper_set_scale_z_max (LightwoodFlipper *self,
                                        gdouble scale);

gdouble lightwood_flipper_get_forward_flip_start_angle (LightwoodFlipper *self);
void lightwood_flipper_set_forward_flip_start_angle (LightwoodFlipper *self,
                                                     gdouble angle);

gdouble lightwood_flipper_get_forward_flip_end_angle (LightwoodFlipper *self);
void lightwood_flipper_set_forward_flip_end_angle (LightwoodFlipper *self,
                                                   gdouble angle);

gdouble lightwood_flipper_get_backward_flip_start_angle (LightwoodFlipper *self);
void lightwood_flipper_set_backward_flip_start_angle (LightwoodFlipper *self,
                                                      gdouble angle);

gdouble lightwood_flipper_get_backward_flip_end_angle (LightwoodFlipper *self);
void lightwood_flipper_set_backward_flip_end_angle (LightwoodFlipper *self,
                                                    gdouble angle);

guint lightwood_flipper_get_first_actor_mark (LightwoodFlipper *self);
void lightwood_flipper_set_first_actor_mark (LightwoodFlipper *self,
                                             guint mark);

guint lightwood_flipper_get_second_actor_mark (LightwoodFlipper *self);
void lightwood_flipper_set_second_actor_mark (LightwoodFlipper *self,
                                              guint mark);

gboolean lightwood_flipper_get_animation_in_progress (LightwoodFlipper *self);
ClutterActor *lightwood_flipper_get_actor_being_animated (LightwoodFlipper *self);

gfloat lightwood_flipper_get_x_pivot_point (LightwoodFlipper *self);
void lightwood_flipper_set_x_pivot_point (LightwoodFlipper *self,
                                          gfloat pivot);

gfloat lightwood_flipper_get_y_pivot_point (LightwoodFlipper *self);
void lightwood_flipper_set_y_pivot_point (LightwoodFlipper *self,
                                          gfloat pivot);

gfloat lightwood_flipper_get_discrete_factor (LightwoodFlipper *self);
void lightwood_flipper_set_discrete_factor (LightwoodFlipper *pLightwoodFlipper,
                                            gfloat fltDiscreteFactor);

G_END_DECLS

#endif /* _FLIPPER_H */
/*** END OF FILE *************************************************************/
