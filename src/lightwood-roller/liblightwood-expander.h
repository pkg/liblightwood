/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


#ifndef LIGHTWOOD_EXPANDER_H
#define LIGHTWOOD_EXPANDER_H

#include <glib-object.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_EXPANDER lightwood_expander_get_type()

#define LIGHTWOOD_EXPANDER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LIGHTWOOD_TYPE_EXPANDER, LightwoodExpander))

#define LIGHTWOOD_EXPANDER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LIGHTWOOD_TYPE_EXPANDER, LightwoodExpanderClass))

#define LIGHTWOOD_IS_EXPANDER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LIGHTWOOD_TYPE_EXPANDER))

#define LIGHTWOOD_IS_EXPANDER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LIGHTWOOD_TYPE_EXPANDER))

#define LIGHTWOOD_EXPANDER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LIGHTWOOD_TYPE_EXPANDER, LightwoodExpanderClass))

typedef struct _LightwoodExpander LightwoodExpander;
typedef struct _LightwoodExpanderClass LightwoodExpanderClass;
typedef struct _LightwoodExpanderPrivate LightwoodExpanderPrivate;

struct _LightwoodExpander
{
  /*< private >*/
  MxWidget parent;

  LightwoodExpanderPrivate *priv;
};

struct _LightwoodExpanderClass
{
  /*< private >*/
  MxWidgetClass parent_class;
};

GType lightwood_expander_get_type (void) G_GNUC_CONST;

ClutterActor *lightwood_expander_new (void);

gfloat lightwood_expander_get_expand_factor (LightwoodExpander *self);
void lightwood_expander_set_expand_factor (LightwoodExpander *self,
                                           gfloat factor);

G_END_DECLS

#endif /* LIGHTWOOD_EXPANDER_H */
