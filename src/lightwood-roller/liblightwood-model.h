/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _LIGHTWOOD_MODEL_H
#define _LIGHTWOOD_MODEL_H

#include <clutter/clutter.h>
#include <thornbury/thornbury.h>

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_MODEL lightwood_model_get_type()
G_DECLARE_DERIVABLE_TYPE (LightwoodModel, lightwood_model, LIGHTWOOD, MODEL, ThornburyModel)

struct _LightwoodModelClass
{
  /*< private >*/
  ThornburyModelClass parent_class;
};

G_END_DECLS

#endif /* _LIGHTWOOD_MODEL_H */
