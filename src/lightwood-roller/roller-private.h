/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _LIGHTWOOD_ROLLER_PRIVATE_H
#define _LIGHTWOOD_ROLLER_PRIVATE_H

#include <liblightwood-roller.h>

#define ROLLER_DEBUG(...) g_debug ( __VA_ARGS__)

typedef struct
{
  gchar *name;
  gint   column;
} AttributeData;

GSList *lightwood_roller_get_attributes (LightwoodRoller *roller);

gdouble lightwood_roller_get_normalized_adjustment_position (LightwoodRoller *roller);

#endif /* _LIGHTWOOD_ROLLER_PRIVATE_H */
