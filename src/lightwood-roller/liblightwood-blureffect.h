/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef LIGHTWOOD_BLUR_EFFECT_H
#define LIGHTWOOD_BLUR_EFFECT_H

#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_BLUR_EFFECT lightwood_blur_effect_get_type()

#define LIGHTWOOD_BLUR_EFFECT(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LIGHTWOOD_TYPE_BLUR_EFFECT, LightwoodBlurEffect))

#define LIGHTWOOD_BLUR_EFFECT_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LIGHTWOOD_TYPE_BLUR_EFFECT, LightwoodBlurEffectClass))

#define LIGHTWOOD_IS_BLUR_EFFECT(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LIGHTWOOD_TYPE_BLUR_EFFECT))

#define LIGHTWOOD_IS_BLUR_EFFECT_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LIGHTWOOD_TYPE_BLUR_EFFECT))

#define LIGHTWOOD_BLUR_EFFECT_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LIGHTWOOD_TYPE_BLUR_EFFECT, LightwoodBlurEffectClass))

typedef struct _LightwoodBlurEffect LightwoodBlurEffect;
typedef struct _LightwoodBlurEffectClass LightwoodBlurEffectClass;
typedef struct _LightwoodBlurEffectPrivate LightwoodBlurEffectPrivate;

struct _LightwoodBlurEffect
{
  /*< private >*/
  ClutterShaderEffect parent;

  LightwoodBlurEffectPrivate *priv;
};

struct _LightwoodBlurEffectClass
{
  /*< private >*/
  ClutterShaderEffectClass parent_class;
};

GType lightwood_blur_effect_get_type (void) G_GNUC_CONST;

ClutterEffect *lightwood_blur_effect_new (void);

void lightwood_blur_effect_set_steps (LightwoodBlurEffect *blur_effect, guint steps);

G_END_DECLS

#endif /* LIGHTWOOD_BLUR_EFFECT_H */
