/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:liblightwood-expander
 * @Title:LightwoodExpander
 * @short_description: Widget providing similar functionality as #MxExpander
 * but with a different UI.
 */

#include "liblightwood-expander.h"

//static void clutter_container_iface_init (ClutterContainerIface *iface);

G_DEFINE_TYPE (LightwoodExpander, lightwood_expander, MX_TYPE_WIDGET)

#define EXPANDER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), LIGHTWOOD_TYPE_EXPANDER, LightwoodExpanderPrivate))

#define ANIMATION_DURATION 500

struct _LightwoodExpanderPrivate
{
  ClutterActor *handle;
  ClutterActor *arrow;
  gboolean expanded;

  ClutterTimeline *timeline;
  ClutterAlpha *alpha;
  gfloat expand_factor;
  ClutterActor *child;
};

enum
{
  PROP_0,

  PROP_EXPAND_FACTOR,
};

static void
get_property (GObject    *object,
                           guint       property_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  LightwoodExpanderPrivate *priv = LIGHTWOOD_EXPANDER (object)->priv;

  switch (property_id)
    {
    case PROP_EXPAND_FACTOR:
      g_value_set_float (value, priv->expand_factor);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
set_property (GObject      *object,
                           guint         property_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  LightwoodExpander *self = LIGHTWOOD_EXPANDER (object);

  switch (property_id)
    {
    case PROP_EXPAND_FACTOR:
      lightwood_expander_set_expand_factor (self, g_value_get_float (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
dispose (GObject *object)
{
  LightwoodExpanderPrivate *priv = LIGHTWOOD_EXPANDER (object)->priv;

  if (priv->handle)
    {
      clutter_actor_remove_child (CLUTTER_ACTOR (object), priv->handle);
      priv->handle = NULL;
    }

  if (priv->timeline)
    {
      g_clear_object (&priv->timeline);
    }

  G_OBJECT_CLASS (lightwood_expander_parent_class)->dispose (object);
}

static void
finalize (GObject *object)
{
  G_OBJECT_CLASS (lightwood_expander_parent_class)->finalize (object);
}

/* ClutterActor implementation */

static void
actor_allocate (ClutterActor          *actor,
                const ClutterActorBox *box,
                ClutterAllocationFlags flags)
{
  LightwoodExpanderPrivate *priv = LIGHTWOOD_EXPANDER (actor)->priv;
  ClutterActorBox child_box;
  gfloat handle_height, child_height;

  CLUTTER_ACTOR_CLASS (lightwood_expander_parent_class)->allocate (actor, box, flags);

  clutter_actor_get_preferred_height (priv->handle, -1, NULL, &handle_height);

  child_box.x1 = 0;
  child_box.x2 = box->x2 - box->x1;
  child_box.y1 = 0;
  child_box.y2 = handle_height;

  clutter_actor_allocate (priv->handle, &child_box, flags);

  if (priv->child != NULL && clutter_actor_is_visible (priv->child))
    {
      clutter_actor_get_preferred_height (priv->child, -1, NULL, &child_height);

      child_box.x1 = 0;
      child_box.x2 = box->x2 - box->x1;
      child_box.y1 = handle_height;
      child_box.y2 = child_box.y1 + child_height * priv->expand_factor;

      clutter_actor_allocate (priv->child, &child_box, flags);
    }
}

static void
actor_paint (ClutterActor *actor)
{
  LightwoodExpanderPrivate *priv = LIGHTWOOD_EXPANDER (actor)->priv;

  CLUTTER_ACTOR_CLASS (lightwood_expander_parent_class)->paint (actor);

  clutter_actor_paint (priv->handle);

  if (priv->expanded && priv->child)
    clutter_actor_paint (priv->child);
}

static void
actor_pick (ClutterActor *actor,
            const ClutterColor *pick_color)
{
  LightwoodExpanderPrivate *priv = LIGHTWOOD_EXPANDER (actor)->priv;

  CLUTTER_ACTOR_CLASS (lightwood_expander_parent_class)->pick (actor, pick_color);

  clutter_actor_paint (priv->handle);

  if (priv->expanded && priv->child)
    clutter_actor_paint (priv->child);
}

static void
actor_map (ClutterActor *actor)
{
  LightwoodExpanderPrivate *priv = LIGHTWOOD_EXPANDER (actor)->priv;

  CLUTTER_ACTOR_CLASS (lightwood_expander_parent_class)->map (actor);

  clutter_actor_map (priv->handle);
}

static void
actor_unmap (ClutterActor *actor)
{
  LightwoodExpanderPrivate *priv = LIGHTWOOD_EXPANDER (actor)->priv;

  CLUTTER_ACTOR_CLASS (lightwood_expander_parent_class)->unmap (actor);

  clutter_actor_unmap (priv->handle);
}

static void
actor_get_preferred_width (ClutterActor *actor,
                           gfloat        for_height,
                           gfloat       *minimal_width_p,
                           gfloat       *natural_width_p)
{
  LightwoodExpanderPrivate *priv = LIGHTWOOD_EXPANDER (actor)->priv;
  gfloat handle_minimal_width, child_minimal_width;
  gfloat handle_natural_width, child_natural_width;

  clutter_actor_get_preferred_width (priv->handle, -1, &handle_minimal_width,
                                     &handle_natural_width);

  if (priv->child != NULL && clutter_actor_is_visible (priv->child))
    clutter_actor_get_preferred_width (priv->child, -1, &child_minimal_width,
                                       &child_natural_width);
  else
    {
      child_minimal_width = 0.0;
      child_natural_width = 0.0;
    }

  child_minimal_width *= priv->expand_factor;
  child_natural_width *= priv->expand_factor;

  if (minimal_width_p != NULL)
    *minimal_width_p = handle_minimal_width + child_minimal_width;

  if (natural_width_p != NULL)
    *natural_width_p = handle_natural_width + child_natural_width;
}

static void
actor_get_preferred_height (ClutterActor *actor,
                            gfloat        for_width,
                            gfloat       *minimal_height_p,
                            gfloat       *natural_height_p)
{
  LightwoodExpanderPrivate *priv = LIGHTWOOD_EXPANDER (actor)->priv;
  gfloat handle_minimal_height, child_minimal_height;
  gfloat handle_natural_height, child_natural_height;

  clutter_actor_get_preferred_height (priv->handle, -1, &handle_minimal_height,
                                     &handle_natural_height);

  if (priv->child != NULL && clutter_actor_is_visible (priv->child))
    clutter_actor_get_preferred_height (priv->child, -1, &child_minimal_height,
                                        &child_natural_height);
  else
    {
      child_minimal_height = 0.0;
      child_natural_height = 0.0;
    }

  child_minimal_height *= priv->expand_factor;
  child_natural_height *= priv->expand_factor;

  if (minimal_height_p != NULL)
    *minimal_height_p = handle_minimal_height + child_minimal_height;

  if (natural_height_p != NULL)
    *natural_height_p = handle_natural_height + child_natural_height;
}

static void
set_expanded (LightwoodExpander *expander, gboolean expanded, gboolean animate)
{
  LightwoodExpanderPrivate *priv = expander->priv;

  if (priv->expanded == expanded)
    return;

  priv->expanded = expanded;

  if (priv->child == NULL)
    return;

  if (priv->expanded)
    {
      if (animate)
        {
          clutter_timeline_set_direction (priv->timeline, CLUTTER_TIMELINE_FORWARD);
          clutter_timeline_start (priv->timeline);
        }
      else
        g_object_set (expander, "expand-factor", 1.0, NULL);

      clutter_actor_show (priv->child);
    }
  else
    {
      if (animate)
        {
          clutter_timeline_set_direction (priv->timeline, CLUTTER_TIMELINE_BACKWARD);
          clutter_timeline_start (priv->timeline);
        }
      else
        {
          g_object_set (expander, "expand-factor", 0.0, NULL);
          clutter_actor_hide (priv->child);
        }
    }
}

static void
handle_clicked_cb (ClutterClickAction *action,
                   ClutterActor       *handle,
                   LightwoodExpander        *expander)
{
  LightwoodExpanderPrivate *priv = expander->priv;

  if (clutter_timeline_is_playing (priv->timeline))
    return;

  g_debug ("handle_clicked_cb");

  set_expanded (expander, !priv->expanded, TRUE);
}

static gboolean
handle_gesture_progress_cb (ClutterGestureAction *action,
                            ClutterActor         *handle,
                            LightwoodExpander          *expander)
{
  LightwoodExpanderPrivate *priv = expander->priv;
  gfloat child_height, press_y, y, expand_factor;

  if (priv->child == NULL)
    return FALSE;

  clutter_actor_get_preferred_height (priv->child, -1, NULL, &child_height);

  clutter_gesture_action_get_press_coords (action, 0, NULL, &press_y);
  clutter_gesture_action_get_motion_coords (action, 0, NULL, &y);

  if (priv->expanded)
    expand_factor = 1.0 - (y - press_y) / child_height;
  else
    expand_factor = (press_y - y) / child_height;

  expand_factor = MAX (expand_factor, 0.0);
  expand_factor = MIN (expand_factor, 1.0);
  clutter_actor_show (priv->child);

  g_object_set (expander, "expand-factor", expand_factor, NULL);

  return TRUE;
}

static gboolean
handle_gesture_end_cb (ClutterGestureAction *action,
                       ClutterActor         *handle,
                       LightwoodExpander          *expander)
{
  LightwoodExpanderPrivate *priv = expander->priv;
  gfloat press_y, y, delta;
  gint drag_threshold;
  MxSettings *settings;
  ClutterActor *child = NULL;

  if (child == NULL)
    return FALSE;

  clutter_gesture_action_get_press_coords (action, 0, NULL, &press_y);
  clutter_gesture_action_get_release_coords (action, 0, NULL, &y);

  delta = ABS (press_y - y);

  settings = mx_settings_get_default ();
  g_object_get (G_OBJECT (settings), "drag-threshold", &drag_threshold, NULL);

  if (priv->expanded)
    {
      if (delta >= drag_threshold)
        set_expanded (expander, FALSE, TRUE);
      else
        {
          clutter_timeline_set_direction (priv->timeline, CLUTTER_TIMELINE_FORWARD);
          clutter_timeline_start (priv->timeline);
        }
    }
  else
    {
      if (delta >= drag_threshold)
        set_expanded (expander, TRUE, TRUE);
      else
        {
          clutter_timeline_set_direction (priv->timeline, CLUTTER_TIMELINE_BACKWARD);
          clutter_timeline_start (priv->timeline);
        }
    }

  return TRUE;
}

static void
timeline_completed_cb (ClutterTimeline *timeline, LightwoodExpander *expander)
{
  LightwoodExpanderPrivate *priv = expander->priv;

  if (priv->child != NULL)
    {
      if (priv->expanded)
        clutter_actor_show (priv->child);
      else
        clutter_actor_hide (priv->child);
    }

  if (priv->expanded)
    clutter_actor_set_name (priv->arrow, "Lightwood-expander-arrow-down");
  else
    clutter_actor_set_name (priv->arrow, "Lightwood-expander-arrow-up");
}

static void
timeline_new_frame_cb (ClutterTimeline *timeline,
                       gint             frame_num,
                       LightwoodExpander     *expander)
{
  LightwoodExpanderPrivate *priv = expander->priv;
  gdouble progress = clutter_timeline_get_progress (timeline);

  if (priv->expanded)
    g_object_set (expander, "expand-factor", MAX (progress, priv->expand_factor), NULL);
  else
    g_object_set (expander, "expand-factor", MIN (progress, priv->expand_factor), NULL);
}

static void
actor_added (ClutterActor *container, ClutterActor *child)
{
  LightwoodExpanderPrivate *priv = LIGHTWOOD_EXPANDER (container)->priv;

  if (MX_IS_TOOLTIP (child))
    return;

  if (priv->child)
    clutter_actor_remove_child (container, priv->child);

  priv->child = child;
  if (!priv->expanded)
    clutter_actor_hide (priv->child);
}

static void
actor_removed (ClutterActor *container, ClutterActor *child)
{
 // LightwoodExpanderPrivate *priv = LIGHTWOOD_EXPANDER (container)->priv;
}

static void
lightwood_expander_class_init (LightwoodExpanderClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);
  GParamSpec *pspec;

  g_type_class_add_private (klass, sizeof (LightwoodExpanderPrivate));

  object_class->get_property = get_property;
  object_class->set_property = set_property;
  object_class->dispose = dispose;
  object_class->finalize = finalize;

  actor_class->allocate = actor_allocate;
  actor_class->get_preferred_width = actor_get_preferred_width;
  actor_class->get_preferred_height = actor_get_preferred_height;
  actor_class->paint = actor_paint;
  actor_class->pick = actor_pick;
  actor_class->map = actor_map;
  actor_class->unmap = actor_unmap;

  pspec = g_param_spec_float ("expand-factor",
                              "expand factor",
                              "Factor of the expansion",
                              0.0, 1.0,
                              0.0,
                              G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_EXPAND_FACTOR, pspec);
}

static void
lightwood_expander_init (LightwoodExpander *self)
{
  ClutterAction *action;
  GError *error = NULL;

  self->priv = EXPANDER_PRIVATE (self);

  mx_style_load_from_file (mx_style_get_default (),
                           PACKAGE_DATA_DIR "/Lightwoodroller/style/default.css",
                           &error);

  if (error)
    {
      g_warning (G_STRLOC ": Error loading style: %s", error->message);
      g_error_free (error);
    }

  self->priv->handle = mx_box_layout_new ();
  clutter_actor_set_reactive (CLUTTER_ACTOR (self->priv->handle), TRUE);
  clutter_actor_add_child (CLUTTER_ACTOR (self), self->priv->handle);
  clutter_actor_show (self->priv->handle);

  action = clutter_click_action_new ();
  clutter_actor_add_action (self->priv->handle, action);
  g_signal_connect (action, "clicked", G_CALLBACK (handle_clicked_cb), self);

  action = clutter_gesture_action_new ();
  clutter_actor_add_action (self->priv->handle, action);
  g_signal_connect (action, "gesture-progress", G_CALLBACK (handle_gesture_progress_cb), self);
  g_signal_connect (action, "gesture-end", G_CALLBACK (handle_gesture_end_cb), self);

  self->priv->arrow = mx_icon_new ();
  clutter_actor_set_name (self->priv->arrow, "Lightwood-expander-arrow-up");
  mx_box_layout_insert_actor_with_properties (MX_BOX_LAYOUT (self->priv->handle),
                                              self->priv->arrow,
                                              -1,
                                              "x-align", MX_ALIGN_MIDDLE,
                                              "x-fill", FALSE,
                                              "expand", TRUE,
                                              NULL);
  clutter_actor_show (self->priv->arrow);

  self->priv->timeline = clutter_timeline_new (ANIMATION_DURATION);
  //clutter_timeline_set_progress_mode (self->priv->timeline, CLUTTER_EASE_IN_SINE);
  g_signal_connect (self->priv->timeline, "completed",
                    G_CALLBACK (timeline_completed_cb), self);
  g_signal_connect (self->priv->timeline, "new-frame",
                    G_CALLBACK (timeline_new_frame_cb), self);

  g_signal_connect (self, "actor-added", G_CALLBACK (actor_added), NULL);
  g_signal_connect (self, "actor-removed", G_CALLBACK (actor_removed), NULL);
}

/**
 * lightwood_expander_new:
 *
 * Create a new #LightwoodExpander
 *
 * Returns: (transfer full): a newly allocated #LightwoodExpander
 */
ClutterActor *
lightwood_expander_new (void)
{
  return g_object_new (LIGHTWOOD_TYPE_EXPANDER, NULL);
}

/**
 * lightwood_expander_get_expand_factor:
 * @self: a #LightwoodExpander
 *
 * Return the #LightwoodExpander:expand-factor property
 *
 * Returns: the value of #LightwoodExpander:expand-factor property
 */
gfloat
lightwood_expander_get_expand_factor (LightwoodExpander *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_EXPANDER (self), 0.0);

  return self->priv->expand_factor;
}

/**
 * lightwood_expander_set_expand_factor:
 * @self: a #LightwoodExpander
 * @factor: the new value
 *
 * Update the #LightwoodExpander:expand-factor property
 */
void
lightwood_expander_set_expand_factor (LightwoodExpander *self,
                                      gfloat factor)
{
  g_return_if_fail (LIGHTWOOD_IS_EXPANDER (self));

  if (self->priv->expand_factor == factor)
    return;

  self->priv->expand_factor = factor;
  clutter_actor_queue_relayout (CLUTTER_ACTOR (self));
  g_object_notify (G_OBJECT (self), "expand-factor");
}
