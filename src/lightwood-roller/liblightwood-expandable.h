/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


#ifndef _LIGHTWOOD_EXPANDABLE_H
#define _LIGHTWOOD_EXPANDABLE_H

#include <glib-object.h>

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_EXPANDABLE lightwood_expandable_get_type()

#define LIGHTWOOD_EXPANDABLE(obj)    (G_TYPE_CHECK_INSTANCE_CAST ((obj), LIGHTWOOD_TYPE_EXPANDABLE, LightwoodExpandable))

#define LIGHTWOOD_IS_EXPANDABLE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LIGHTWOOD_TYPE_EXPANDABLE))

#define LIGHTWOOD_EXPANDABLE_GET_INTERFACE(inst) (G_TYPE_INSTANCE_GET_INTERFACE ((inst), LIGHTWOOD_TYPE_EXPANDABLE, LightwoodExpandableIface))

typedef struct _LightwoodExpandable      LightwoodExpandable; /* dummy */
typedef struct _LightwoodExpandableIface LightwoodExpandableIface;

/**
 * LightwoodExpandableIface:
 * @set_expanded: called when the actor becomes expanded
 *
 * Interface to be implemented by actors that can become expanded.
 */
struct _LightwoodExpandableIface
{
 /*< private >*/
 GObjectClass parent_class;

 /*< public >*/
 void (*set_expanded) (LightwoodExpandable *expandable,
                        gboolean       expanded,
                        gboolean       animated);
};

GType lightwood_expandable_get_type (void) G_GNUC_CONST;

void
lightwood_expandable_set_expanded (LightwoodExpandable *expandable,
                             gboolean       expanded,
                             gboolean       animated);

G_END_DECLS

#endif /* _LIGHTWOOD_EXPANDABLE_H */
