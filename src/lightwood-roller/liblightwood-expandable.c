/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:liblightwood-expandable
 * @Title:LightwoodExpandable
 * @short_description:Interface to be implemented by items of #LightwoodFixedRoller
 * that can be expanded.
 *
 * By implementing #LightwoodExpandable::set_expanded, actors will be notified by the
 * roller container of when they should become expanded or unexpanded.
 * 
 */

#include "liblightwood-expandable.h"

typedef LightwoodExpandableIface LightwoodExpandableInterface;
G_DEFINE_INTERFACE (LightwoodExpandable, lightwood_expandable, G_TYPE_OBJECT);

static void
lightwood_expandable_default_init (LightwoodExpandableInterface *iface)
{

}

/**
 * lightwood_expandable_set_expanded:
 * @expandable: A #LightwoodExpandable.
 * @expanded: Whether the item becomes expanded or unexpanded.
 * @animated: Whether the item should perform an animation when expanding or
 * unexpanding.
 *
 * Change the state.
 */
void
lightwood_expandable_set_expanded (LightwoodExpandable *expandable,
                             gboolean       expanded,
                             gboolean       animated)
{
  LightwoodExpandableInterface *iface;

  g_return_if_fail (LIGHTWOOD_IS_EXPANDABLE (expandable));

  iface = LIGHTWOOD_EXPANDABLE_GET_INTERFACE (expandable);

  if (iface->set_expanded)
    iface->set_expanded (expandable, expanded, animated);
}
