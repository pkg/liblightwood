/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

 /**
 * SECTION:liblightwood-deformturn
 * @Title:LightwoodDeformPageTurn
 * @Short_Description: Effect that deforms a texture as if it folded.Page turn like animation.
 * @See_Also: #ClutterActor
 *
 * #LightwoodDeformPageTurn will provides an page turn like animation. 
 *
 * To create the bended cornered texture use lightwood_deform_page_turn_new() to instatiate #LightwoodDeformPageTurn and to get the right bended 
 * corner lightwood_deform_page_turn_bend_left_corner() and for left lightwood_deform_page_turn_bend_right_corner().
 *
  */

#include "liblightwood-deformturn.h"

#include <math.h>

#define SHADOW_WIDTH 30.
#define CORNER_SIZE 40.

G_DEFINE_TYPE (LightwoodDeformPageTurn, lightwood_deform_page_turn, CLUTTER_TYPE_DEFORM_EFFECT)

#define DEFORM_PAGE_TURN_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), LIGHTWOOD_TYPE_DEFORM_PAGE_TURN, LightwoodDeformPageTurnPrivate))

struct _LightwoodDeformPageTurnPrivate
{
  gdouble angle;
  gboolean inverted;
  gboolean bend_left_corner;
  gboolean bend_right_corner;
};

enum
{
  PROP_0,

  PROP_INVERTED,
  PROP_ANGLE,
};

static void
set_inverted (LightwoodDeformPageTurn *page_turn,
              gboolean           inverted)
{
  LightwoodDeformPageTurnPrivate *priv;

  g_return_if_fail (LIGHTWOOD_IS_DEFORM_PAGE_TURN (page_turn));

  priv = page_turn->priv;

  if (priv->inverted != inverted)
    {
      priv->inverted = inverted;
      g_object_notify (G_OBJECT (page_turn), "inverted");
      clutter_deform_effect_invalidate (CLUTTER_DEFORM_EFFECT (page_turn));
    }
}

static void
set_angle (LightwoodDeformPageTurn *page_turn,
           gdouble            angle)
{
  LightwoodDeformPageTurnPrivate *priv;

  g_return_if_fail (LIGHTWOOD_IS_DEFORM_PAGE_TURN (page_turn));

  priv = page_turn->priv;

  if (priv->angle != angle)
    {
      priv->angle = angle;
      g_object_notify (G_OBJECT (page_turn), "angle");
      clutter_deform_effect_invalidate (CLUTTER_DEFORM_EFFECT (page_turn));
    }
}

static void
lightwood_deform_page_turn_get_property (GObject    *object,
                                   guint       property_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
  LightwoodDeformPageTurnPrivate *priv = LIGHTWOOD_DEFORM_PAGE_TURN (object)->priv;
  switch (property_id)
    {
    case PROP_ANGLE:
      g_value_set_double (value, priv->angle);
      break;

    case PROP_INVERTED:
      g_value_set_boolean (value, priv->inverted);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
lightwood_deform_page_turn_set_property (GObject      *object,
                                   guint         property_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
  switch (property_id)
    {
    case PROP_INVERTED:
      set_inverted (LIGHTWOOD_DEFORM_PAGE_TURN (object), g_value_get_boolean (value));
      break;

    case PROP_ANGLE:
      set_angle (LIGHTWOOD_DEFORM_PAGE_TURN (object), g_value_get_double (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
calculate_position (LightwoodDeformPageTurn *texture,
                    gfloat             width,
                    gfloat            *x,
                    gfloat            *z)
{
  LightwoodDeformPageTurnPrivate *priv = texture->priv;
  gfloat new_x, new_z;

  *x = floorf (*x);

  if (*x >= width / 2 && !priv->inverted)
    {
      if (priv->angle < G_PI / 2)
        {
          new_x = width / 2 + cos (priv->angle) * (*x - width / 2);
          new_z = sin (priv->angle) * (*x - width / 2);
        }
      else
        {
          new_x = width / 2 - cos (G_PI - priv->angle) * (*x - width / 2);
          new_z = sin (G_PI - priv->angle) * (*x - width / 2);
        }
    }
  else if (*x <= width / 2 && priv->inverted)
    {
      if (priv->angle < G_PI / 2)
        {
          new_x = width / 2 - cos (priv->angle) * (width / 2 - *x);
          new_z = sin (priv->angle) * (width / 2 - *x);
        }
      else
        {
          new_x = width / 2 + cos (G_PI - priv->angle) * (width / 2 - *x);
          new_z = sin (G_PI - priv->angle) * (width / 2 - *x);
        }
    }
  else
    return;

  *x = new_x;
  if (z != NULL)
    {
      *z = (new_z + *z) / 32;  /* So the page stays in the screen */
      *z += 1.; /* To reduce z-fighting */
    }
}

static void
_set_vertex (LightwoodDeformPageTurn *effect, CoglTextureVertex *vertex,
             CoglColor *color, gfloat x, gfloat y)
{
  ClutterActor *actor = clutter_actor_meta_get_actor (CLUTTER_ACTOR_META (effect));
  ClutterActorBox box;

  clutter_actor_get_allocation_box (actor, &box);

  vertex->x = x; vertex->y = y; vertex->z = 0;
  vertex->color = *color;
  calculate_position (LIGHTWOOD_DEFORM_PAGE_TURN (effect), box.x2 - box.x1,
                      &vertex->x, &vertex->z);
}

static void
paint_shadow (LightwoodDeformPageTurn *effect)
{
  LightwoodDeformPageTurnPrivate *priv = effect->priv;
  ClutterActor *actor = clutter_actor_meta_get_actor (CLUTTER_ACTOR_META (effect));
  ClutterActorBox box;
  CoglTextureVertex vertices[6];
  CoglColor solid, transparent;
  gfloat width, height;

  cogl_color_init_from_4f (&solid, 0.0, 0.0, 0.0, 1.0);
  cogl_color_init_from_4f (&transparent, 0.0, 0.0, 0.0, 0.0);

  clutter_actor_get_allocation_box (actor, &box);
  clutter_actor_box_get_size (&box, &width, &height);

  /* Left shadow */
  _set_vertex (effect, &vertices[0], &transparent, -SHADOW_WIDTH, CORNER_SIZE);
  if (priv->bend_left_corner)
    {
      _set_vertex (effect, &vertices[1], &transparent, CORNER_SIZE - SHADOW_WIDTH, 0);
      _set_vertex (effect, &vertices[2], &solid, CORNER_SIZE, 0);
    }
  else
    {
      _set_vertex (effect, &vertices[1], &transparent, -SHADOW_WIDTH, 0);
      _set_vertex (effect, &vertices[2], &solid, 0, 0);
    }
  _set_vertex (effect, &vertices[3], &solid, 0, CORNER_SIZE);
  _set_vertex (effect, &vertices[4], &solid, 0, height);
  _set_vertex (effect, &vertices[5], &transparent, -SHADOW_WIDTH, height);

  cogl_polygon (vertices, 6, TRUE);

  /* Right shadow */
  _set_vertex (effect, &vertices[0], &transparent, width + SHADOW_WIDTH, CORNER_SIZE);
  _set_vertex (effect, &vertices[1], &transparent, width + SHADOW_WIDTH, height);
  _set_vertex (effect, &vertices[2], &solid, width, height);
  _set_vertex (effect, &vertices[3], &solid, width, CORNER_SIZE);
  if (priv->bend_right_corner)
    {
      _set_vertex (effect, &vertices[4], &solid, width - CORNER_SIZE, 0);
      _set_vertex (effect, &vertices[5], &transparent, width - CORNER_SIZE + SHADOW_WIDTH, 0);
    }
  else
    {
      _set_vertex (effect, &vertices[4], &solid, width, 0);
      _set_vertex (effect, &vertices[5], &transparent, width + SHADOW_WIDTH, 0);
    }

  cogl_polygon (vertices, 6, TRUE);
}

static void
paint_gloss (LightwoodDeformPageTurn *effect)
{
  LightwoodDeformPageTurnPrivate *priv = effect->priv;
  ClutterActor *actor = clutter_actor_meta_get_actor (CLUTTER_ACTOR_META (effect));
  ClutterActorBox box;
  CoglTextureVertex vertices[4];
  CoglColor solid, transparent;
  gfloat x, z, width, height, shade;

  shade = 0.5 * sin (priv->angle) + 0.05;
  cogl_color_init_from_4f (&solid, shade, shade, shade, 0.1);
  cogl_color_init_from_4f (&transparent, 0.0, 0.0, 0.0, 0.0);

  clutter_actor_get_allocation_box (actor, &box);

  width = box.x2 - box.x1;
  height = box.y2 - box.y1;

  if (priv->inverted)
    x = 0;
  else
    x = width;

  z = 0.;
  calculate_position (LIGHTWOOD_DEFORM_PAGE_TURN (effect), width, &x, &z);

  if ((!priv->inverted && priv->angle < G_PI / 2.0) ||
      (priv->inverted && priv->angle > G_PI / 2.0))
    {
      vertices[0].x = width / 2; vertices[0].y = 0; vertices[0].z = 0;
      vertices[0].color = solid;

      vertices[1].x = x; vertices[1].y = 0; vertices[1].z = z;
      vertices[1].color = transparent;

      vertices[2].x = x; vertices[2].y = height; vertices[2].z = z;
      vertices[2].color = transparent;

      vertices[3].x = width / 2; vertices[3].y = height; vertices[3].z = 0;
      vertices[3].color = solid;
    }
  else
    {
      vertices[0].x = x; vertices[0].y = 0; vertices[0].z = z;
      vertices[0].color = transparent;

      vertices[1].x = width / 2; vertices[1].y = 0; vertices[1].z = z;
      vertices[1].color = solid;

      vertices[2].x = width / 2; vertices[2].y = height; vertices[2].z = z;
      vertices[2].color = solid;

      vertices[3].x = x; vertices[3].y = height; vertices[3].z = z;
      vertices[3].color = transparent;
    }

  cogl_polygon (vertices, 4, TRUE);
}

static void
paint_corner_bend (LightwoodDeformPageTurn *effect)
{
  LightwoodDeformPageTurnPrivate *priv = effect->priv;
  ClutterActor *actor = clutter_actor_meta_get_actor (CLUTTER_ACTOR_META (effect));
  ClutterActorBox box;
  CoglTextureVertex vertices[3];
  CoglColor white, black;
  gfloat  width, height;

  cogl_color_init_from_4f (&white, 1., 1., 1., 1.);
  cogl_color_init_from_4f (&black, 0., 0., 0., 1.);

  clutter_actor_get_allocation_box (actor, &box);
  clutter_actor_box_get_size (&box, &width, &height);

  /* Left corner */
  if (priv->bend_left_corner)
    {
      _set_vertex (effect, &vertices[0], &black, CORNER_SIZE, 0);
      _set_vertex (effect, &vertices[1], &white, CORNER_SIZE, CORNER_SIZE);
      _set_vertex (effect, &vertices[2], &black, 0, CORNER_SIZE);

      cogl_polygon (vertices, 3, TRUE);
    }

  /* Right corner */
  if (priv->bend_right_corner)
    {
      _set_vertex (effect, &vertices[0], &black, width - CORNER_SIZE, 0);
      _set_vertex (effect, &vertices[1], &black, width, CORNER_SIZE);
      _set_vertex (effect, &vertices[2], &white, width - CORNER_SIZE, CORNER_SIZE);

      cogl_polygon (vertices, 3, TRUE);
    }
}

static void
paint (ClutterEffect *effect, ClutterEffectPaintFlags flags)
{
  LightwoodDeformPageTurnPrivate *priv = LIGHTWOOD_DEFORM_PAGE_TURN (effect)->priv;

  CLUTTER_EFFECT_CLASS (lightwood_deform_page_turn_parent_class)->paint (effect, flags);

  cogl_set_source_color4ub (0, 0, 0, 0);

  if (priv->angle != 0)
    paint_gloss (LIGHTWOOD_DEFORM_PAGE_TURN (effect));

  paint_corner_bend (LIGHTWOOD_DEFORM_PAGE_TURN (effect));

  paint_shadow (LIGHTWOOD_DEFORM_PAGE_TURN (effect));
}

static void
deform_vertex (ClutterDeformEffect *effect,
               gfloat               width,
               gfloat               height,
               CoglTextureVertex   *vertex)
{
  LightwoodDeformPageTurnPrivate *priv = LIGHTWOOD_DEFORM_PAGE_TURN (effect)->priv;
  gfloat left_corner_edge = CORNER_SIZE - vertex->y;
  gfloat right_corner_edge = width - CORNER_SIZE + vertex->y;

  left_corner_edge += 5.;
  right_corner_edge -= 3.;

  if (vertex->x < left_corner_edge &&
      vertex->y < CORNER_SIZE &&
      priv->bend_left_corner)
    vertex->x = left_corner_edge;
  else if (vertex->x > right_corner_edge &&
           vertex->y < CORNER_SIZE &&
           priv->bend_right_corner)
    vertex->x = right_corner_edge;

  calculate_position (LIGHTWOOD_DEFORM_PAGE_TURN (effect), width, &vertex->x,
                                                            &vertex->z);
}

static void
lightwood_deform_page_turn_class_init (LightwoodDeformPageTurnClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ClutterEffectClass *effect_class = CLUTTER_EFFECT_CLASS (klass);
  ClutterDeformEffectClass *deform_class = CLUTTER_DEFORM_EFFECT_CLASS (klass);
  GParamSpec *pspec;

  g_type_class_add_private (klass, sizeof (LightwoodDeformPageTurnPrivate));

  object_class->get_property = lightwood_deform_page_turn_get_property;
  object_class->set_property = lightwood_deform_page_turn_set_property;

  effect_class->paint = paint;

  deform_class->deform_vertex = deform_vertex;

  pspec = g_param_spec_boolean ("inverted",
                                "Inverted",
                                "Whether the page turn starts from the left side",
                                FALSE,
                                G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_INVERTED, pspec);

  pspec = g_param_spec_double ("angle",
                               "Angle",
                               "Angle of the surface being turned",
                               0.0, G_PI, 0.0,
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_ANGLE, pspec);
}

static void
lightwood_deform_page_turn_init (LightwoodDeformPageTurn *self)
{
  self->priv = DEFORM_PAGE_TURN_PRIVATE (self);
}

/**
 * lightwood_deform_page_turn_new:
 *
 * Create a new #LightwoodDeformPageTurn
 *
 * Returns: (transfer full): a newly allocated #LightwoodDeformPageTurn
 */
LightwoodDeformPageTurn *
lightwood_deform_page_turn_new (void)
{
  return g_object_new (LIGHTWOOD_TYPE_DEFORM_PAGE_TURN, NULL);
}
/**
 * lightwood_deform_page_turn_bend_left_corner:
 * @effect: object
 * @enabled: %TRUE/%FALSE,if TURE enabled else diabled.
 *
 * left corners of the texture will be bended to indicate 
 * deform page turn.
 *
 */
void
lightwood_deform_page_turn_bend_left_corner (LightwoodDeformPageTurn *effect,
                                       gboolean enabled)
{
  LightwoodDeformPageTurnPrivate *priv = effect->priv;

  if (priv->bend_left_corner == enabled)
    return;

  priv->bend_left_corner = enabled;
  clutter_effect_queue_repaint (CLUTTER_EFFECT (effect));
}
/**
 * lightwood_deform_page_turn_bend_right_corner:
 * @effect: object
 * @enabled: %TRUE/%FALSE,if TURE enabled else diabled.
 *
 * right corners of the texture will be bended to indicate 
 * deform page turn.
 *
 */

void
lightwood_deform_page_turn_bend_right_corner (LightwoodDeformPageTurn *effect,
                                        gboolean enabled)
{
  LightwoodDeformPageTurnPrivate *priv = effect->priv;

  if (priv->bend_right_corner == enabled)
    return;

  priv->bend_right_corner = enabled;
  clutter_effect_queue_repaint (CLUTTER_EFFECT (effect));
}

/**
 * lightwood_deform_page_turn_get_angle:
 * @self: a #LightwoodDeformPageTurn
 *
 * Return the #LightwoodDeformPageTurn:angle property
 *
 * Returns: the value of #LightwoodDeformPageTurn:angle property
 */
gdouble
lightwood_deform_page_turn_get_angle (LightwoodDeformPageTurn *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_DEFORM_PAGE_TURN (self), 0.0);

  return self->priv->angle;
}

/**
 * lightwood_deform_page_turn_set_angle:
 * @self: a #LightwoodDeformPageTurn
 * @angle: the new angle value
 *
 * Update the #LightwoodDeformPageTurn:angle property
 */
void
lightwood_deform_page_turn_set_angle (LightwoodDeformPageTurn *self,
                                      gdouble angle)
{
  g_return_if_fail (LIGHTWOOD_IS_DEFORM_PAGE_TURN (self));

  set_angle (self, angle);
}

/**
 * lightwood_deform_page_turn_get_inverted:
 * @self: a #LightwoodDeformPageTurn
 *
 * Return the #LightwoodDeformPageTurn:inverted property
 *
 * Returns: the value of #LightwoodDeformPageTurn:inverted property
 */
gboolean
lightwood_deform_page_turn_get_inverted (LightwoodDeformPageTurn *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_DEFORM_PAGE_TURN (self), FALSE);

  return self->priv->inverted;
}

/**
 * lightwood_deform_page_turn_set_inverted:
 * @self: a #LightwoodDeformPageTurn
 * @inverted: the new inverted value
 *
 * Update the #LightwoodDeformPageTurn:inverted property
 */
void
lightwood_deform_page_turn_set_inverted (LightwoodDeformPageTurn *self,
                                         gboolean inverted)
{
  g_return_if_fail (LIGHTWOOD_IS_DEFORM_PAGE_TURN (self));

  set_inverted (self, inverted);
}
