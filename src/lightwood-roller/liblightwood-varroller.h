/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _LIGHTWOOD_VARIABLE_ROLLER_H
#define _LIGHTWOOD_VARIABLE_ROLLER_H

#include "liblightwood-roller.h"

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_VARIABLE_ROLLER lightwood_variable_roller_get_type()

#define LIGHTWOOD_VARIABLE_ROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LIGHTWOOD_TYPE_VARIABLE_ROLLER, LightwoodVariableRoller))

#define LIGHTWOOD_VARIABLE_ROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LIGHTWOOD_TYPE_VARIABLE_ROLLER, LightwoodVariableRollerClass))

#define LIGHTWOOD_IS_VARIABLE_ROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LIGHTWOOD_TYPE_VARIABLE_ROLLER))

#define LIGHTWOOD_IS_VARIABLE_ROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LIGHTWOOD_TYPE_VARIABLE_ROLLER))

#define LIGHTWOOD_VARIABLE_ROLLER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LIGHTWOOD_TYPE_VARIABLE_ROLLER, LightwoodVariableRollerClass))

typedef struct _LightwoodVariableRoller LightwoodVariableRoller;
typedef struct _LightwoodVariableRollerClass LightwoodVariableRollerClass;
typedef struct _LightwoodVariableRollerPrivate LightwoodVariableRollerPrivate;

struct _LightwoodVariableRoller
{
  /*< private >*/
  LightwoodRoller parent;

  LightwoodVariableRollerPrivate *priv;
};

struct _LightwoodVariableRollerClass
{
  /*< private >*/
  LightwoodRollerClass parent_class;
};

GType lightwood_variable_roller_get_type (void) G_GNUC_CONST;

ClutterActor *lightwood_variable_roller_new (void);

/* Properties */

gfloat lightwood_variable_roller_get_height (LightwoodVariableRoller *self);
void lightwood_variable_roller_set_height (LightwoodVariableRoller *self,
                                           gfloat height);

G_END_DECLS

#endif /* _LIGHTWOOD_VARIABLE_ROLLER_H */
