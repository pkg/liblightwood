/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef LIGHTWOOD_CYLINDER_DEFORM_EFFECT_H
#define LIGHTWOOD_CYLINDER_DEFORM_EFFECT_H

#include <clutter/clutter.h>

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_CYLINDER_DEFORM_EFFECT lightwood_cylinder_deform_effect_get_type()

#define LIGHTWOOD_CYLINDER_DEFORM_EFFECT(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LIGHTWOOD_TYPE_CYLINDER_DEFORM_EFFECT, LightwoodCylinderDeformEffect))

#define LIGHTWOOD_CYLINDER_DEFORM_EFFECT_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LIGHTWOOD_TYPE_CYLINDER_DEFORM_EFFECT, LightwoodCylinderDeformEffectClass))

#define LIGHTWOOD_IS_CYLINDER_DEFORM_EFFECT(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LIGHTWOOD_TYPE_CYLINDER_DEFORM_EFFECT))

#define LIGHTWOOD_IS_CYLINDER_DEFORM_EFFECT_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LIGHTWOOD_TYPE_CYLINDER_DEFORM_EFFECT))

#define LIGHTWOOD_CYLINDER_DEFORM_EFFECT_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LIGHTWOOD_TYPE_CYLINDER_DEFORM_EFFECT, LightwoodCylinderDeformEffectClass))

typedef struct _LightwoodCylinderDeformEffect LightwoodCylinderDeformEffect;
typedef struct _LightwoodCylinderDeformEffectClass LightwoodCylinderDeformEffectClass;

struct _LightwoodCylinderDeformEffect
{
  /*< private >*/
  ClutterDeformEffect parent;
};

struct _LightwoodCylinderDeformEffectClass
{
  /*< private >*/
  ClutterDeformEffectClass parent_class;
};

GType lightwood_cylinder_deform_effect_get_type (void) G_GNUC_CONST;

ClutterEffect *lightwood_cylinder_deform_effect_new (void);

G_END_DECLS

#endif /* LIGHTWOOD_CYLINDER_DEFORM_EFFECT_H */
