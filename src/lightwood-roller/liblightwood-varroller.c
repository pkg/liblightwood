/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:liblightwood-varroller
 * @Title:LightwoodVariableRoller
 * @short_description:  #LightwoodRoller subclass that respects the size of each of its
 * contained actors at the cost of less scalability.
 * 
 * #LightwoodVariableRoller,as name suggests any height of the roller item can fit into the roller.
 * This roller is less efficient campare to #LightwoodFixedRoller.Because #LightwoodVariableRoller will
 * create the items as many as it is asked to, there are no optimization. Hence as the number 
 * of the object in roller increases, chances are hight that it may become slow and glitchy.
 * The way this roller allocates its children are totally different from #LightwoodFixedRoller.
 */
/*******************************************************************************
 * Fixes
 * Description					        Date 			    Author

 *1) Animatioon if number of items 	|	21-01-2013		|   Ramya Ajiri
     are less than visible items
 *2) fix given fro crash while roller |	25-02-2013		|	Ramya Ajiri
 *   is getting destroyed.
*3) cc resolved 				|27-Aug-2013		|Ramya
********************************************************************************/
#include "liblightwood-varroller.h"
#include "roller-private.h"
#include "roller-internal.h"

#include <math.h>

G_DEFINE_TYPE (LightwoodVariableRoller, lightwood_variable_roller, LIGHTWOOD_TYPE_ROLLER)

#define VARIABLE_ROLLER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), LIGHTWOOD_TYPE_VARIABLE_ROLLER, LightwoodVariableRollerPrivate))

struct _LightwoodVariableRollerPrivate
{
  GSList *children;
/* height of roller*/
	gfloat height;
};
enum {
  PROP_0,
  PROP_HEIGHT,//property height

};


/* ClutterActor implementation */


/* GObject implementation */

static void
lightwood_variable_roller_dispose (GObject *object)
{
  G_OBJECT_CLASS (lightwood_variable_roller_parent_class)->dispose (object);
}

static void
lightwood_variable_roller_finalize (GObject *object)
{
  G_OBJECT_CLASS (lightwood_variable_roller_parent_class)->finalize (object);
}

/* ClutterActor implementation */

static void
actor_get_preferred_width (ClutterActor *actor,
                           gfloat        for_height,
                           gfloat       *min_width_p,
                           gfloat       *natural_width_p)
{
  LightwoodVariableRollerPrivate *priv = LIGHTWOOD_VARIABLE_ROLLER (actor)->priv;
  GSList *iter;

  if (min_width_p != NULL)
    *min_width_p = 0;

  if (natural_width_p != NULL)
    *natural_width_p = 0;

  for (iter = priv->children; iter != NULL; iter = g_slist_next (iter))
    {
      ClutterActor *child_actor = (ClutterActor *) iter->data;
      gfloat min_width, natural_width;

      clutter_actor_get_preferred_width (child_actor, -1, &min_width,
                                         &natural_width);

      if (min_width_p != NULL && min_width > *min_width_p)
        *min_width_p = min_width;

      if (natural_width_p != NULL && natural_width > *natural_width_p)
        *natural_width_p = natural_width;
    }
}

static void
_get_child_height(ClutterActor *child_actor,gfloat for_width,gfloat **min_height_p,gfloat **natural_height_p)
{
  gfloat min_height, natural_height;

  clutter_actor_get_preferred_height (child_actor, for_width, &min_height,
      &natural_height);

  if (*min_height_p != NULL)
    **min_height_p += min_height;

  if (*natural_height_p != NULL)
    **natural_height_p += natural_height;	
}
static void
actor_get_preferred_height (ClutterActor *actor,
                            gfloat        for_width,
                            gfloat       *min_height_p,
                            gfloat       *natural_height_p)
{
  LightwoodVariableRollerPrivate *priv = LIGHTWOOD_VARIABLE_ROLLER (actor)->priv;
  GSList *iter;

  if (min_height_p != NULL)
    *min_height_p = 0;

  if (natural_height_p != NULL)
    *natural_height_p = 0;

  for (iter = priv->children; iter != NULL; iter = g_slist_next (iter))
  {
    ClutterActor *child_actor = (ClutterActor *) iter->data;
    _get_child_height(child_actor,for_width,&min_height_p,&natural_height_p);
  }
  /* if it is a short roller, keep min height as the height of the container*/
  if(priv->height > *natural_height_p)
    *natural_height_p = priv->height;
}

/* LightwoodRoller implementation */

static guint
roller_get_row_from_coord (LightwoodRoller *roller, gfloat x, gfloat y)
{
  LightwoodVariableRollerPrivate *priv = LIGHTWOOD_VARIABLE_ROLLER (roller)->priv;
  GSList *iter;
  guint row;
  gfloat next_y, total_height;
  gboolean roll_over_enabled;

  g_object_get (G_OBJECT (roller), "roll-over", &roll_over_enabled, NULL);

  clutter_actor_get_preferred_height (CLUTTER_ACTOR (roller), -1, NULL,
                                      &total_height);

  y = ceilf (y);

  if (roll_over_enabled)
    {
      while (y < 0)
        y += total_height;
      while (y > total_height)
        y -= total_height;
    }

  next_y = 0.0;
  for (iter = priv->children, row = 0;
       iter != NULL;
       iter = g_slist_next (iter), row++)
    {
      ClutterActor *child_actor = (ClutterActor *) iter->data;
      gfloat item_height, y1, y2;

      clutter_actor_get_preferred_height (child_actor, -1, NULL, &item_height);

      y1 = next_y;
      y2 = next_y + item_height;
      next_y = y2;

      if (y1 <= y && y2 >= y)
        return row;
    }

  g_warning ("No row at position %f", y);

  return 0;
}

static GSList *
roller_get_visible_items (LightwoodRoller *roller,
                          gfloat     forced_roller_width,
                          gfloat     forced_item_width,
                          gfloat    *first_x,
                          gfloat    *first_y)
{
  LightwoodVariableRollerPrivate *priv = LIGHTWOOD_VARIABLE_ROLLER (roller)->priv;
  ClutterActorBox box;
  GSList *iter, *visible_children;
  gdouble start;
  gfloat next_y, available_height;
  gboolean first_y_is_set;
  gboolean roll_over_enabled;

  g_object_get (G_OBJECT (roller), "roll-over", &roll_over_enabled, NULL);

  clutter_actor_get_allocation_box (CLUTTER_ACTOR (roller), &box);
  available_height = box.y2 - box.y1;
  start = lightwood_roller_get_normalized_adjustment_position (roller);

  if (first_x != NULL)
    *first_x = 0.0;

  first_y_is_set = FALSE;
  next_y = 0.0;
  visible_children = NULL;
  for (iter = priv->children;
       iter != NULL;
       iter = g_slist_next (iter))
    {
      ClutterActor *child_actor = (ClutterActor *) iter->data;
      gfloat item_height, y1, y2;

      clutter_actor_get_preferred_height (child_actor, -1, NULL, &item_height);

      y1 = next_y;
      y2 = next_y + item_height;
      next_y = y2;

      if (y1 <= start + available_height && y2 >= start)
        {
          visible_children = g_slist_append (visible_children, child_actor);

          if (first_y != NULL && !first_y_is_set)
            {
              *first_y = y1 - start;
              first_y_is_set = TRUE;
            }
        }
    }

  if (roll_over_enabled)
    {
      gfloat total_height;
      gfloat min_height;
      clutter_actor_get_preferred_height (CLUTTER_ACTOR (roller), -1, &min_height,
                                          &total_height);
/* if short roller,roll-over effect is not required */
		if( priv->height > min_height )
			return visible_children;
      start -= total_height;
      next_y = 0.0;
      for (iter = priv->children;
           iter != NULL;
           iter = g_slist_next (iter))
        {
          ClutterActor *child_actor = (ClutterActor *) iter->data;
          gfloat item_height, y1, y2;

          clutter_actor_get_preferred_height (child_actor, -1, NULL, &item_height);

          y1 = next_y;
          y2 = next_y + item_height;
          next_y = y2;

          if (y1 <= start + available_height && y2 >= start)
            visible_children = g_slist_append (visible_children, child_actor);
        }
    }

  return visible_children;
}

static ClutterActor *
roller_get_item_from_row (LightwoodRoller *roller,
                          guint      row,
                          gfloat     forced_roller_width,
                          gfloat     forced_item_width,
                          gfloat     forced_item_height,
                          gfloat    *y)
{
  LightwoodVariableRollerPrivate *priv = LIGHTWOOD_VARIABLE_ROLLER (roller)->priv;
  ClutterActor *child;

  g_return_val_if_fail (forced_roller_width == -1, NULL);
  g_return_val_if_fail (forced_item_width == -1, NULL);
  g_return_val_if_fail (forced_item_height == -1, NULL);

  if (y != NULL)
    {
      gfloat next_y = 0.0;
      guint current_row = 0;
      GSList *iter;

      for (iter = priv->children;
           iter != NULL;
           iter = g_slist_next (iter), current_row++)
        {
          ClutterActor *child_actor = (ClutterActor *) iter->data;
          gfloat item_height, y1, y2;

          clutter_actor_get_preferred_height (child_actor, -1, NULL, &item_height);

          y1 = next_y;
          y2 = next_y + item_height;
          next_y = y2;

          if (current_row == row)
            {
              *y = y1;
              break;
            }
        }
    }

  child = CLUTTER_ACTOR (g_slist_nth_data (priv->children, row));

  return g_object_ref (child);
}

static gint
roller_get_row_from_item (LightwoodRoller    *roller,
                          ClutterActor *child)
{
  LightwoodVariableRollerPrivate *priv = LIGHTWOOD_VARIABLE_ROLLER (roller)->priv;
  GSList *iter;
  gint i;

  for (iter = priv->children, i = 0; iter; iter = iter->next, i++)
    if (iter->data == child)
      return i;

  return -1;
}

static gfloat
roller_get_item_height (LightwoodRoller *roller, ClutterActor *actor)
{
  gfloat item_height;
	/* to avoid warning if child is NULL */
	if(actor == NULL)
		return 1.0;
  clutter_actor_get_preferred_height (actor, -1, NULL, &item_height);
  return item_height;
}

static gfloat
roller_get_item_width (LightwoodRoller *roller, ClutterActor *actor)
{
  return clutter_actor_get_width (CLUTTER_ACTOR (roller));
}

static gfloat
roller_get_total_height (LightwoodRoller *roller,
                         gfloat     forced_roller_width,
                         gfloat     forced_item_width,
                         gfloat     forced_item_height)
{
  gfloat natural_height;

  g_return_val_if_fail (forced_roller_width == -1, 0);
  g_return_val_if_fail (forced_item_width == -1, 0);
  g_return_val_if_fail (forced_item_height == -1, 0);

  actor_get_preferred_height (CLUTTER_ACTOR (roller), -1, NULL, &natural_height);

  return natural_height;
}

static void
recreate_children (LightwoodVariableRoller *roller)
{
  LightwoodVariableRollerPrivate *priv = roller->priv;
  ThornburyModel *model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (roller));
  MxItemFactory *item_factory = lightwood_roller_get_factory (LIGHTWOOD_ROLLER (roller));
  ThornburyModelIter *iter;
  GSList *attributes = lightwood_roller_get_attributes (LIGHTWOOD_ROLLER (roller));
  GSList *attr_iter;

  if (priv->children != NULL)
  {
    g_slist_free_full (priv->children, g_object_unref);
    priv->children = NULL;
  }

  if (model == NULL)
    return;

  if (item_factory == NULL)
    return;

  iter = thornbury_model_get_first_iter (model);
  if(NULL == iter)
    return;
  g_print("model length = %d\n",thornbury_model_get_n_rows(model));
  while (!thornbury_model_iter_is_last (iter))
  {
    ClutterActor *child = mx_item_factory_create (item_factory);
    //	clutter_actor_push_internal (CLUTTER_ACTOR (roller));
    //    clutter_actor_set_parent (child, CLUTTER_ACTOR (roller));
    //	clutter_actor_pop_internal (CLUTTER_ACTOR (roller));
    clutter_actor_add_child(CLUTTER_ACTOR (roller),child);
    clutter_actor_show (child);

    g_object_freeze_notify (G_OBJECT (child));
    for (attr_iter = attributes; attr_iter; attr_iter = attr_iter->next)
    {
      GValue value = { 0, };
      AttributeData *attr = attr_iter->data;

      thornbury_model_iter_get_value (iter, attr->column, &value);
      g_object_set_property (G_OBJECT (child), attr->name, &value);
      g_value_unset (&value);
    }
    g_object_thaw_notify (G_OBJECT (child));

    priv->children = g_slist_append (priv->children, child);

    iter = thornbury_model_iter_next (iter);
  }
  g_object_unref (iter);
}

static void
notify_model_cb (LightwoodVariableRoller *roller,
                 GParamSpec        *pspec,
                 gpointer           user_data)
{
  recreate_children (roller);
}

static void
notify_factory_cb (LightwoodVariableRoller *roller,
                   GParamSpec        *pspec,
                   gpointer           user_data)
{
  recreate_children (roller);
}

static void
adjustment_value_notify_cb (MxAdjustment    *adjustment,
                            GParamSpec      *pspec,
                            LightwoodFixedRoller  *roller)
{
  clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));
}

static void
get_property (GObject    *object,
              guint       property_id,
              GValue     *value,
              GParamSpec *pspec)
{

	switch (property_id)
	{
		  /* Height of the roller */
		case PROP_HEIGHT:
			g_value_set_float (value, LIGHTWOOD_VARIABLE_ROLLER (object)->priv->height);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;

	}
}

/**
 * lightwood_variable_roller_set_height:
 * @self: a #LightwoodVariableRoller
 * @height: the new value
 *
 * Update the #LightwoodVariableRoller:height property
 */
void
lightwood_variable_roller_set_height (LightwoodVariableRoller *self,
                                      gfloat height)
{
  g_return_if_fail (LIGHTWOOD_IS_VARIABLE_ROLLER (self));

  self->priv->height = height;
  g_object_notify (G_OBJECT (self), "height");
}

static void
set_property (GObject      *object,
                               guint         property_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{

	switch (property_id)
	{
		case PROP_HEIGHT:
			{
				lightwood_variable_roller_set_height(LIGHTWOOD_VARIABLE_ROLLER(object),
						g_value_get_float (value));
			}
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}
static void
lightwood_variable_roller_class_init (LightwoodVariableRollerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);
  LightwoodRollerClass *roller_class = LIGHTWOOD_ROLLER_CLASS (klass);
	GParamSpec *pspec;

  g_type_class_add_private (klass, sizeof (LightwoodVariableRollerPrivate));
	object_class->get_property = get_property;
  object_class->set_property = set_property;
  object_class->dispose = lightwood_variable_roller_dispose;
  object_class->finalize = lightwood_variable_roller_finalize;

  actor_class->get_preferred_width = actor_get_preferred_width;
  actor_class->get_preferred_height = actor_get_preferred_height;

  roller_class->get_row_from_coord = roller_get_row_from_coord;
  roller_class->get_visible_items = roller_get_visible_items;
  roller_class->get_item_from_row = roller_get_item_from_row;
  roller_class->get_row_from_item = roller_get_row_from_item;
  roller_class->get_item_height = roller_get_item_height;
  roller_class->get_item_width = roller_get_item_width;
  roller_class->get_total_height = roller_get_total_height;

	pspec = g_param_spec_float ("height",
                              "Height",
                              "Roller Height",
                              -G_MAXFLOAT,
                              G_MAXFLOAT,
                              0.0,
                              G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_HEIGHT, pspec);
}

static void
lightwood_variable_roller_init (LightwoodVariableRoller *roller)
{
  MxAdjustment *vadjust;

  roller->priv = VARIABLE_ROLLER_PRIVATE (roller);

  mx_scrollable_get_adjustments (MX_SCROLLABLE (roller), NULL, &vadjust);
  g_signal_connect (vadjust, "notify::value",
                    G_CALLBACK (adjustment_value_notify_cb),
                    roller);

  g_signal_connect (G_OBJECT (roller), "notify::model",
                    G_CALLBACK (notify_model_cb), NULL);

  g_signal_connect (G_OBJECT (roller), "notify::factory",
                    G_CALLBACK (notify_factory_cb), NULL);

  clutter_actor_set_clip_to_allocation (CLUTTER_ACTOR (roller), TRUE);
}

/**
 * lightwood_variable_roller_new:
 *
 * Create a new #LightwoodVariableRoller
 *
 * Returns: (transfer full): a newly allocated #LightwoodVariableRoller
 */
ClutterActor *
lightwood_variable_roller_new (void)
{
  return g_object_new (LIGHTWOOD_TYPE_VARIABLE_ROLLER, NULL);
}

/**
 * lightwood_variable_roller_get_height:
 * @self: a #LightwoodVariableRoller
 *
 * Return the #LightwoodVariableRoller:height property
 *
 * Returns: the value of #LightwoodVariableRoller:height property
 *
 * Since: UNRELEASED
 */
gfloat
lightwood_variable_roller_get_height (LightwoodVariableRoller *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_VARIABLE_ROLLER (self), 0.0);

  return self->priv->height;
}
