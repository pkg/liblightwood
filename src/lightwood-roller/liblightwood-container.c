/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-container.c */
/**
 * SECTION:liblightwood-container
 * @Title:LightwoodContainer
 * @short_description: Base Abstrct class for Container.
 * 
 * 
 * #LightwoodContainer is ideally written for general conatiner,In Lightwood it is used as container for roller,
 * because #LightwoodRoller doesnot contain 
 * #MxKineticScrollView which is used for finger scroll and scrollable features.
 * MxKineticScrollView,background etc, for  other custom actors inside it. Hence as a whole it is 
 * called as Roller in variant. It can be used as base roller container for Lightwood variants.
 * 
 *
 */


#include "liblightwood-container.h"


G_DEFINE_ABSTRACT_TYPE (LightwoodContainer, lightwood_container, CLUTTER_TYPE_ACTOR)//MX_TYPE_STACK)//LIGHTWOOD_TYPE_WIDGET)

#define CONTAINER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), LIGHTWOOD_TYPE_CONTAINER, LightwoodContainerPrivate))

struct _LightwoodContainerPrivate
{
	gboolean flag; //dummy variable
};


static void
lightwood_container_get_property (GObject    *object,
                              guint       property_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
lightwood_container_set_property (GObject      *object,
                              guint         property_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
lightwood_container_dispose (GObject *object)
{
  G_OBJECT_CLASS (lightwood_container_parent_class)->dispose (object);
}

static void
lightwood_container_finalize (GObject *object)
{
  G_OBJECT_CLASS (lightwood_container_parent_class)->finalize (object);
}
static void
actor_paint (ClutterActor *actor)
{
  //g_print("%s\n", __FUNCTION__);


  CLUTTER_ACTOR_CLASS (lightwood_container_parent_class)->paint (actor);

}
static void
actor_allocate (ClutterActor          *actor,
                    const ClutterActorBox *box,
                    ClutterAllocationFlags flags)
{
  ClutterActorClass *klass;

  klass = CLUTTER_ACTOR_CLASS (lightwood_container_parent_class);
  klass->allocate (actor, box, flags);
}

static void
lightwood_container_class_init (LightwoodContainerClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);

	g_type_class_add_private (klass, sizeof (LightwoodContainerPrivate));

	object_class->get_property = lightwood_container_get_property;
	object_class->set_property = lightwood_container_set_property;
	object_class->dispose = lightwood_container_dispose;
	object_class->finalize = lightwood_container_finalize;

	actor_class->paint = actor_paint;
	actor_class->allocate = actor_allocate;
}

static void
lightwood_container_init (LightwoodContainer *self)
{
  self->priv = CONTAINER_PRIVATE (self);
}
