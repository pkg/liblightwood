/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-container.h */

#ifndef _LIGHTWOOD_CONTAINER_H
#define _LIGHTWOOD_CONTAINER_H

#include <glib-object.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop
G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_CONTAINER lightwood_container_get_type()

#define LIGHTWOOD_CONTAINER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LIGHTWOOD_TYPE_CONTAINER, LightwoodContainer))

#define LIGHTWOOD_CONTAINER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LIGHTWOOD_TYPE_CONTAINER, LightwoodContainerClass))

#define LIGHTWOOD_IS_CONTAINER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LIGHTWOOD_TYPE_CONTAINER))

#define LIGHTWOOD_IS_CONTAINER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LIGHTWOOD_TYPE_CONTAINER))

#define LIGHTWOOD_CONTAINER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LIGHTWOOD_TYPE_CONTAINER, LightwoodContainerClass))

typedef struct _LightwoodContainer LightwoodContainer;
typedef struct _LightwoodContainerClass LightwoodContainerClass;
typedef struct _LightwoodContainerPrivate LightwoodContainerPrivate;

struct _LightwoodContainer
{
  ClutterActor parent;//MxStack parent;

  LightwoodContainerPrivate *priv;
};

struct _LightwoodContainerClass
{
  ClutterActorClass parent;//MxStackClass parent_class;
};

GType lightwood_container_get_type (void) G_GNUC_CONST;


G_END_DECLS

#endif /* _LIGHTWOOD_CONTAINER_H */
